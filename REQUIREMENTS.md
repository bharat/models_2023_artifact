# Hardware and Software Requirements
## Hardware 
At a minimum, we require 4GB of RAM (due to the L# implementation requiring a 64-bit OS.)
Beyond that, we do not have any other requirements. Any piece of hardware can run our software,
as long as the necessary toolchains (for Rust, Python, and C++) are available. 
Note, however, that our experiments **may take up to four days** to finish (see the running experiments section in the readme).

## Software
We require a 64-bit OS and Docker. 
While Windows, Linux and macOS are all supported, we have tested the software on macOS and Linux only.
We require `Rust` (v1.61 and above), `C++17` (GCC) and `Python` (tested with 3.9 and 3.11). 
Additional packages for `Python` are present in `requirements.txt`.
All required software is downloaded and built automatically on Docker.

## Issues 
Sometimes an experiment produces an incomplete log file; we have been unable to fix this particular issue. 
The code for the logging is fine, as re-running the faulty experiment with the same settings fixes the issue.