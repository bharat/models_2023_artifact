## An Experimental Evaluation of Conformance Testing Techniques in Active Automata Learning (Artifact)
Authors: Bharat Garhewal and Carlos Diego N. Damasceno

### Artifact Availability
The artifact is available at Zenodo via (INSERT-LINK-HERE) with DOI (INSERT-DOI-HERE).
The artifact consists of the requisite software and a Dockerfile to set-up and run the experiments. 

### Directory structure 
The directory structure is as follows (we list only the important files/directories):
```
artifact
    ├── FSMlib 
    └── lsharp
    |   ├── analysis (contains analysis scripts and results)
    |   ├── experiment_models (contains the models used in our experiments)
    |   ├── expr_models.py (python script to run subset of experiments and select models and random seeds)
    |   ├── expr_models_full.py (python script to run all experiments and select models and random seeds)
    |   ├── run_me.sh (main file for running the experiments)
    ├── README.md    
    ├── REQUIREMENTS.md
```
The `run_me.sh` script is the only file you need to run, everything is controlled by the script.

# How-to use the artifact
## Docker 

Assuming you are at the artifact's root directory, please run the following from a terminal:
```bash
# Build the Dockerfile.
docker build -t models_img .
# Run the docker image interactively
docker run -i -it models_img:latest
```
You should now be logged into the docker terminal at in the `/home/sws/artifact/lsharp` directory.

## Running Experiments
Our experiments **take up to four days** to finish on a Ryzen 3700X (8 core) CPU with 32 GB of RAM.
As such, we heavily recommend running a subset of our experiments.
Given that the longest-running processes have a single-threaded implementation 
(in particular, the SPY-(H) implementation, with the algorithm being exponential-time), 
we recommend running with the default experiment models we have selected.

In order to run the full set of experiments, please use the argument `a` as shown below: 
```bash
./run_me.sh 
```
**Note: re-running the experiments will remove all information from a previous run.**

In order to run the full set of experiments, please use the argument `a` as shown below: 
```bash
./run_me.sh -a
```

The results (PDF, CSV, and TeX files) are avaiable in the `artifact/lsharp/analysis/results` directory.
The tables in the paper have been generated from the CSV files.

The output files are named as `{Style}_{Metric}_{s1|s2}.extension` where: 
1. Style : `Fixed` or `Random` or `BoE` (Best-of-Each)
2. Metric: `TC` (Total Cost) or `APFD` 
(The s1/s2 suffixes can be ignored.)

You can copy the `/home/sws/artifact/lsharp/analysis/results` directory to your host machine using Docker's `cp` command.
Please remember that according to our metrics, *lower `TC` is better* and *higher APFD is better*. 
The default CSV files have not been fixed according to our metrics, the tex files contain results in the style mentioned in our paper.