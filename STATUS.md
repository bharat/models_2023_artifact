We believe that our submission qualifies for 
1. Artifacts Available v1.1 (The artifact is available at Zenodo with a DOI), and 
2. Artifacts Evaluated – Reusable v1.1 

We believe that the artifact includes all 
supporting documentation required to re-run the complete experiments.
As our paper is based on the results of these experiments, the artifact is consistent.
Additionally, the experiments are exercisable.