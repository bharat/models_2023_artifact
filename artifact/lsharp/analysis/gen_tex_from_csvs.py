# %%
## Import packages

import pandas as pd

# %%
data_dir = "./data/"
fixed_tc_s1 = data_dir + "Fixed_TC_s1.csv"
fixed_apfd_s2 = data_dir + "Fixed_APFD_s2.csv"
random_tc_s1 = data_dir + "Random_TC_s1.csv"
random_apfd_s2 = data_dir + "Random_APFD_s2.csv"
boe_tc_s1 = data_dir + "BoE_TC_s1.csv"
boe_apfd_s2 = data_dir + "BoE_APFD_s2.csv"
pd.set_option("display.precision", 3)
pd.set_option("styler.format.precision", 3)


def get_table(file, apfd):
    df = pd.read_csv(file)
    df = df.query(f"`mwu`<=0.05")
    df = df[["A", "B", "vda_estimate", "vda_magnitude"]]
    df["vda_estimate"] = df["vda_estimate"].round(decimals=3)
    df = df.rename(
        columns={"vda_estimate": "Effect Size", "vda_magnitude": "Effect Magnitude"}
    )
    df["Effect Size"] = df["Effect Size"] - 0.5
    #     print(df)
    if apfd:
        mask = df["Effect Size"] < 0
    else:
        mask = df["Effect Size"] > 0
    df.loc[mask, ["A", "B"]] = df.loc[mask, ["B", "A"]].values
    #     print(df)
    df["Effect Size"] = df.apply(
        lambda row: row["Effect Size"]
        if row["Effect Size"] > 0
        else row["Effect Size"] * -1,
        axis=1,
    )
    df = df.set_index(["A", "B"])
    df = df.sort_index()
    return df


# %%
## TC tables

pd.set_option("display.float_format", "{:.3f}".format)

for data in [fixed_tc_s1, random_tc_s1, boe_tc_s1]:
    #     print(data)
    df = get_table(data, apfd=False)

    #     print(df.style.to_latex())
    pd.set_option("display.precision", 3)

    tbl = df.style.to_latex()
    with open(data + ".tex", "w") as f:
        f.write(tbl)


# %%
# APFD tables

for data in [fixed_apfd_s2, random_apfd_s2, boe_apfd_s2]:
    #     print(data)
    df = get_table(data, apfd=True)
    pd.set_option("display.precision", 3)

    #     print(df.style.to_latex())
    tbl = df.style.to_latex()
    with open(data + ".tex", "w") as f:
        f.write(tbl)

# %%
