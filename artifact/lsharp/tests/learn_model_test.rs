use std::path::PathBuf;

use assert_cmd::Command;
use serial_test::serial;

/// Test for checking CompTreeState invalidation during CE processing.
#[serial]
#[test]
fn learn_tcp_linux_server() {
    let path: PathBuf = ["./tests", "src_models", "TCP_Linux_Server.dot"]
        .iter()
        .collect();
    let model = path.to_str().unwrap();
    let mut bin = Command::cargo_bin("lsharp").unwrap();
    bin.args([
        "-e",
        "iads",
        "--rule2",
        "ads",
        "--rule3",
        "ads",
        "-m",
        model,
        "-k",
        "2",
        "-l",
        "2",
        "-x",
        "3265",
        "--eq-mode",
        "infinite",
        "-v",
        "0",
        "--out",
        "results_test.csv",
    ])
    .assert()
    .success();
}

#[serial]
#[test]
fn learn_bitvise_model() {
    let path: PathBuf = ["./tests", "src_models", "BitVise.dot"].iter().collect();
    let binding = path.canonicalize().unwrap();
    let file_exists = binding.try_exists();
    assert!(file_exists.is_ok(), "File does not exist!");
    let model = binding.to_str().unwrap();

    let mut bin = Command::cargo_bin("lsharp").unwrap();
    bin.args([
        "-e",
        "hads-int",
        "--rule2",
        "ads",
        "--rule3",
        "ads",
        "-m",
        model,
        "-k",
        "5",
        "-l",
        "10",
        "-x",
        "42",
        "--eq-mode",
        "infinite",
        "-v",
        "0",
        "--out",
        "results.csv",
    ])
    .assert()
    .success();
}

#[serial]
#[test]
fn learn_1_mastercard_fix() {
    let path: PathBuf = ["./tests", "src_models", "1_learnresult_MasterCard_fix.dot"]
        .iter()
        .collect();
    let binding = path.canonicalize().unwrap();
    let model = binding.to_str().unwrap();
    let mut bin = Command::cargo_bin("lsharp").unwrap();
    bin.args([
        "-e",
        "hads-int",
        "--rule2",
        "ads",
        "--rule3",
        "ads",
        "-m",
        model,
        "-k",
        "5",
        "-l",
        "10",
        "-x",
        "42",
        "--eq-mode",
        "infinite",
        "-v",
        "0",
        "--out",
        "results.csv",
    ])
    .assert()
    .success();
}

#[serial]
#[test]
fn learn_tcp_freebsd_server_model() {
    let path: PathBuf = ["./tests", "src_models", "TCP_FreeBSD_Server.dot"]
        .iter()
        .collect();
    let binding = path.canonicalize().unwrap();
    let model = binding.to_str().unwrap();
    let mut bin = Command::cargo_bin("lsharp").unwrap();
    bin.args([
        "-e",
        "hads-int",
        "--rule2",
        "ads",
        "--rule3",
        "ads",
        "-m",
        model,
        "-k",
        "5",
        "-l",
        "10",
        "-x",
        "4",
        "--eq-mode",
        "infinite",
        "-v",
        "0",
        "--out",
        "results.csv",
    ])
    .assert()
    .success();
}
