#!/usr/bin/env bash

# By default, run small set.
all_experiments=false

while getopts 'a' flag; do
  case $flag in
    a)
      all_experiments=true
      ;;
    ?)
      echo "script usage: $(basename \$0) [-a]" >&2
      exit 1
      ;;
  esac
done


::() {
    echo -e "\e[1;33m:: \e[0;32m$*\e[0m" >&2
    "$@"
}

## We have too many "unused" warnings with compiling L#.
export RUSTFLAGS="-Awarnings"

echo "Building L#"
cargo build --release
echo "L# Built!"

echo "Building FSMlib"
:: cd ../FSMlib
:: chmod +x ./build.sh
:: ./build.sh
:: cd ../lsharp

## Clear the old experiment files first.
echo "Removing old output files"
:: rm -rf hypothesis log csvs finite_logs infinite_logs
:: mkdir hypothesis log csvs

## Run and parse the finite mode experiments.
echo "Running finite mode experiments"
if [ "$all_experiments" = true ] ; then 
    :: python3 expr_models_full.py finite
else 
    :: python3 expr_models.py finite
fi
:: mv log finite_logs 
:: mkdir log
echo "Parsing log files"
:: python3 parse_logs.py ./finite_logs finite_logs.csv

## Run and parse the infinite mode experiments.
echo "Running infinite mode experiments"
if [ "$all_experiments" = true ] ; then 
    :: python3 expr_models_full.py infinite
else 
    :: python3 expr_models.py infinite
fi
:: mv log infinite_logs 
:: mkdir log
echo "Parsing log files"
:: python3 parse_logs.py ./infinite_logs infinite_logs.csv

## Analyze the results.
echo "Analysis phase"
:: cd analysis
:: python3 analysis.py
:: python3 gen_tex_from_csvs.py
:: cd results
echo "Copying tex, csv, and PDF files"
:: cp ../data/Fixed_TC_s1.csv.tex .
:: cp ../data/Fixed_TC_s1.csv .
:: cp ../data/Fixed_TC_s1.pdf .
:: cp ../data/Fixed_APFD_s2.csv.tex .
:: cp ../data/Fixed_APFD_s2.csv .
:: cp ../data/Fixed_APFD_s2.pdf .
:: cp ../data/Random_TC_s1.csv.tex .
:: cp ../data/Random_TC_s1.csv .
:: cp ../data/Random_TC_s1.pdf .
:: cp ../data/Random_APFD_s2.csv.tex .
:: cp ../data/Random_APFD_s2.csv .
:: cp ../data/Random_APFD_s2.pdf .
:: cp ../data/BoE_TC_s1.csv.tex .
:: cp ../data/BoE_TC_s1.csv .
:: cp ../data/BoE_TC_s1.pdf .
:: cp ../data/BoE_APFD_s2.csv.tex .
:: cp ../data/BoE_APFD_s2.csv .
:: cp ../data/BoE_APFD_s2.pdf .
:: cd ~/artifact/lsharp
echo "All done, you can copy the /home/sws/artifact/analysis/results directory to your host machine to view the files."

