use std::collections::HashMap;
use std::path::Path;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use lsharp_ru::definitions::mealy::{shortest_separating_sequence, InputSymbol, OutputSymbol};
use lsharp_ru::util::parsers::machine::read_mealy_from_file_using_maps;
use lsharp_ru::{definitions::mealy::Mealy, util::parsers::machine::read_mealy_from_file};

fn load_basic_fsm(
    name: &str,
) -> (
    Mealy,
    HashMap<String, InputSymbol>,
    HashMap<String, OutputSymbol>,
) {
    let file_name = Path::new(name);
    read_mealy_from_file(file_name.to_str().expect("Safe"))
}

fn sss() {
    // let fsm = load_basic_fsm("hyp_esm/hypothesis_534.dot");
    let (fsm, in_map, out_map) = load_basic_fsm("benches/sep_seq_models/esm-manual-controller.dot");
    let hyp = read_mealy_from_file_using_maps(
        "benches/sep_seq_models/esm_hypothesis_534.dot",
        &in_map,
        &out_map,
    )
    .expect("Safe");
    let seq = shortest_separating_sequence(&hyp, &fsm, None, None);
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut grp = c.benchmark_group("SSS");
    grp.sample_size(100);
    grp.bench_function("SSS_100", |b| {
        b.iter(black_box(sss));
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
