import subprocess
from time import sleep
import itertools
import sys

from progress.bar import Bar

model_names = [
    "10_learnresult_MasterCard_fix",
    "GnuTLS_3.3.8_server_regular",
    "Rabo_learnresult_MAESTRO_fix",
    "1_learnresult_MasterCard_fix",
    "NSS_3.17.4_client_full",
    "Rabo_learnresult_SecureCode_Aut_fix",
    "4_learnresult_MAESTRO_fix",
    "NSS_3.17.4_client_regular",
    "4_learnresult_PIN_fix",
    "NSS_3.17.4_server_regular",
    r"4_learnresult_SecureCode Aut_fix",
    "OpenSSH",
    "ASN_learnresult_MAESTRO_fix",
    "OpenSSL_1.0.1g_client_regular",
    r"ASN_learnresult_SecureCode Aut_fix",
    "OpenSSL_1.0.1g_server_regular",
    "BitVise",
    "OpenSSL_1.0.1j_client_regular",
    "DropBear",
    "OpenSSL_1.0.1j_server_regular",
    "Volksbank_learnresult_MAESTRO_fix",
    "GnuTLS_3.3.12_client_full",
    "OpenSSL_1.0.1l_client_regular",
    "learnresult_fix",
    "GnuTLS_3.3.12_client_regular",
    "OpenSSL_1.0.1l_server_regular",
    "miTLS_0.1.3_server_regular",
    "GnuTLS_3.3.12_server_full",
    "OpenSSL_1.0.2_client_full",
    "model1",
    "GnuTLS_3.3.12_server_regular",
    "OpenSSL_1.0.2_client_regular",
    "model3",
    "GnuTLS_3.3.8_client_full",
    "OpenSSL_1.0.2_server_regular",
    "model4",
    "GnuTLS_3.3.8_client_regular",
    "RSA_BSAFE_C_4.0.4_server_regular",
    "GnuTLS_3.3.8_server_full",
    "RSA_BSAFE_Java_6.1.1_server_regular",
    "TCP_FreeBSD_Client",
    "TCP_FreeBSD_Server",
    "TCP_Linux_Client",
    "TCP_Linux_Server",
    "TCP_Windows8_Client",
    "TCP_Windows8_Server",
]

seeds = [
    81,
    100,
    158,
    216,
    245,
    359,
    366,
    470,
    560,
    578,
    580,
    597,
    661,
    689,
    692,
    783,
    818,
    879,
    930,
    968,
    995,
    1004,
    1005,
    1190,
    1205,
    1257,
    1320,
    1534,
    1541,
    1596,
    1607,
    1665,
    1836,
    1989,
    2015,
    2143,
    2147,
    2199,
    2221,
    2263,
    2283,
    2365,
    2370,
    2408,
    2495,
    2528,
    2554,
    2558,
    2561,
    2588,
    2610,
]


common_oracles = ["hads-int", "hsi", "w", "wp", "iads"]
finite_only_oracles = ["soucha-spy", "soucha-spyh", "soucha-h"]


def get_lsharp_command(name, oracle, seed, es, rnd_len, mode):
    org_dir = "./experiment_models/"
    model = org_dir + name + ".dot"
    return [
        "./target/release/lsharp",
        "-e",
        oracle,
        "--rule2",
        "sep-seq",
        "--rule3",
        "sep-seq",
        "-k",
        str(es),
        "-l",
        str(rnd_len),
        "-x",
        str(seed),
        "--eq-mode",
        mode,
        "-m",
        model,
        "-v",
        "1",
        "--out",
        "./csvs/results.csv" + name + str(seed) + oracle,
        "-q",
        "--compress-tree",
    ]


def run_expr(cmd):
    pr = subprocess.Popen(cmd)
    if pr.wait() != 0:
        Exception("Experiment failed!")
        exit(-1)
    return


### Construct the infinite-mode experiment commands.
def infinite_only_experiments():
    es = [1, 2]
    rnd_lens = [2, 3]
    oracles = common_oracles
    cmds = []
    for k, oracle, model, seed, rnd_len in itertools.product(
        es, oracles, model_names, seeds, rnd_lens
    ):
        cmd = get_lsharp_command(model, oracle, seed, k, rnd_len, "infinite")
        cmds.append(cmd)
    return cmds


### Construct the finite-mode experiment commands.
def finite_only_experiments():
    es = [1, 2]
    rnd_lens = [2]  # This value does not really matter in finite mode.
    oracles = common_oracles + finite_only_oracles
    random_seed = [81]
    cmds = []
    for k, oracle, model, seed, rnd_len in itertools.product(
        es, oracles, model_names, random_seed, rnd_lens
    ):
        cmd = get_lsharp_command(model, oracle, seed, k, rnd_len, "finite")
        cmds.append(cmd)
    return cmds


if __name__ == "__main__":
    if len(sys.argv) != 2:
        raise Exception(
            "Too many/few arguments provided. Please specify either `finite` or `infinite`."
        )
    mode = sys.argv[1]
    if mode not in ["finite", "infinite"]:
        raise Exception(
            "Please specify either `finite` or `infinite` as a positional argument."
        )
    cmds = []
    if mode == "infinite":
        cmds = infinite_only_experiments()
    else:
        cmds = finite_only_experiments()
    with Bar("Running experiments", max=len(cmds), suffix="%(percent)d%%") as bar:
        for cmd in cmds:
            run_expr(cmd)
            bar.next()
