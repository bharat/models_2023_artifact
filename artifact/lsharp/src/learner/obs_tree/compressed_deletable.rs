use std::fmt::Debug;
use std::sync::{Arc, RwLock};

use datasize::DataSize;
use rustc_hash::FxHashMap;

use crate::definitions::mealy::{InputSymbol, OutputSymbol};
use crate::util::toolbox;

use super::ObservationTree;

/// A `Node` can have two forms: a `Normal` have at least two children,
/// and a `Compressed` Node has at most one child.
#[derive(Debug, Clone, DataSize, derive_more::IsVariant)]
pub enum ObsNode {
    /// `Normal` nodes are used when we have two or more children.
    Norm(NormalNode),
    /// `Compressed` nodes are used when we have at most one child.
    Comp(Compressed),
}

impl Default for ObsNode {
    fn default() -> Self {
        Self::Norm(NormalNode::default())
    }
}

#[derive(Debug, Clone, Default, DataSize)]
pub struct NormalNode {
    // The value of the map is the output symbol and the index of the next node.
    data: FxHashMap<InputSymbol, (OutputSymbol, Arc<RwLock<ObsNode>>)>,
}

#[derive(Debug, Default, Clone, DataSize)]
pub struct Compressed {
    data: Vec<(InputSymbol, OutputSymbol)>,
    /// Assigning anything else other than a `NormalNode` state will lead to UB!
    ///
    /// The general idea is that as soon as a compressed node "ends", i.e., when the
    /// `data` sequence in the compressed node is finished, we mark the next node with
    /// this field. As you can imagine, this field is only meant to be used when we
    /// have a compressed node which points to a  normal node; in other words, that
    /// there exists a path in the observation tree which then splits.
    next_node: Option<Arc<RwLock<ObsNode>>>,
}

#[derive(Debug, Default, Clone)]
pub struct TreeState {
    node: Arc<RwLock<ObsNode>>,
    offset: u32,
}

#[derive(Debug, DataSize)]
pub struct ObsTree {
    root: Arc<RwLock<ObsNode>>,
    input_size: usize,
}
impl ObservationTree<InputSymbol, OutputSymbol> for ObsTree {
    type S = TreeState;

    fn insert_observation(
        &mut self,
        start: Option<Self::S>,
        input_seq: &[InputSymbol],
        output_seq: &[OutputSymbol],
    ) -> Self::S {
        let mut curr = start.unwrap_or_else(|| TreeState {
            node: Arc::clone(&self.root),
            offset: 0,
        });
        for (i, o) in Iterator::zip(input_seq.iter(), output_seq.iter()) {
            curr = self.add_transition_get_destination(curr, *i, *o);
        }
        curr
    }

    fn get_access_seq(&self, state: Self::S) -> Vec<InputSymbol> {
        let root = TreeState {
            node: Arc::clone(&self.root),
            offset: 0,
        };
        self.get_transfer_seq(state, root)
    }

    fn get_transfer_seq(&self, to_state: Self::S, from_state: Self::S) -> Vec<InputSymbol> {
        todo!()
    }

    fn get_observation(
        &self,
        start: Option<Self::S>,
        input_seq: &[InputSymbol],
    ) -> Option<Vec<OutputSymbol>> {
        let mut s = start.unwrap_or_else(|| self.initial());
        let mut out_vec = Vec::with_capacity(input_seq.len());
        for &i in input_seq {
            let (o, d) = self.get_out_succ(s, i)?;
            out_vec.push(o);
            s = d;
        }
        Some(out_vec)
    }

    fn get_out_succ(&self, src: Self::S, input: InputSymbol) -> Option<(OutputSymbol, Self::S)> {
        todo!()
    }

    fn get_succ(&self, src: Self::S, input: &[InputSymbol]) -> Option<Self::S> {
        input.iter().try_fold(src, |s, i| self._get_succ(s, *i))
    }

    fn no_succ_defined(&self, basis: &[Self::S], sort: bool) -> Vec<(Self::S, InputSymbol)> {
        let inputs = toolbox::inputs_iterator(self.input_size);
        let mut ret: Vec<_> =
            itertools::Itertools::cartesian_product(basis.iter().cloned(), inputs)
                .filter(|(bs, i)| self._get_succ(bs.clone(), *i).is_none())
                .collect();
        let acc_len = |s| self.get_access_seq(s).len();
        let len_cmp = |a, b| acc_len(a).cmp(&acc_len(b));
        if sort {
            ret.sort_unstable_by(|(a, _), (b, _)| len_cmp(a.clone(), b.clone()));
        }
        ret
    }

    fn size(&self) -> usize {
        todo!("Write function to count all nodes in this tree.");
    }

    fn input_size(&self) -> usize {
        self.input_size
    }

    fn tree_and_hyp_states_apart_sink(
        &self,
        s_t: Self::S,
        s_h: crate::definitions::mealy::State,
        fsm: &crate::definitions::mealy::Mealy,
        sink_output: OutputSymbol,
        depth: usize,
    ) -> bool {
        todo!()
    }
}

impl ObsTree {
    fn initial(&self) -> TreeState {
        TreeState {
            node: Arc::clone(&self.root),
            offset: 0,
        }
    }

    fn add_transition_get_destination(
        &self,
        curr: TreeState,
        i: InputSymbol,
        o: OutputSymbol,
    ) -> TreeState {
        todo!()
    }

    fn _get_succ(&self, s: TreeState, i: InputSymbol) -> Option<TreeState> {
        todo!()
    }
}
