use std::collections::VecDeque;

use itertools::Itertools;

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::util::toolbox;

use super::obs_tree::ObservationTree;

/// Provides a witness between the two states, if one exists.
///
/// If witness is not needed, use [`states_are_apart`].
#[inline]
#[must_use]
pub fn compute_witness<Tree>(tree: &Tree, s1: Tree::S, s2: Tree::S) -> Option<Vec<InputSymbol>>
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    shows_states_are_apart(tree, s1.clone(), s2).map(|d1| tree.get_transfer_seq(d1, s1))
}

/// `true` if states are apart.
///
/// If a witness is needed, use [`compute_witness`].
#[inline]
#[must_use]
pub fn states_are_apart<Tree>(tree: &Tree, s1: Tree::S, s2: Tree::S) -> bool
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    shows_states_are_apart(tree, s1, s2).is_some()
}

pub fn acc_states_are_apart<Tree>(tree: &Tree, s1_a: &[InputSymbol], s2_a: &[InputSymbol]) -> bool
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    let s1 = tree.get_succ(Tree::S::default(), s1_a).expect("Safe");
    let s2 = tree.get_succ(Tree::S::default(), s2_a).expect("Safe");
    states_are_apart(tree, s1, s2)
}

/// Check if a tree and a hypothesis state is apart.
/// Note that this function is not equivalent to  
/// [`states_are_apart`], as an observation tree is *partial*,
/// while FSMs are *complete*.
#[inline]
#[must_use]
pub fn tree_and_hyp_states_apart<Tree>(tree: &Tree, s_t: Tree::S, s_h: State, fsm: &Mealy) -> bool
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    tree_and_hyp_shows_states_are_apart(tree, s_t, s_h, fsm).is_some()
}

pub fn tree_and_hyp_states_apart_sunk_bounded<Tree>(
    tree: &Tree,
    s_t: Tree::S,
    s_h: State,
    fsm: &Mealy,
    sink_output: OutputSymbol,
    depth: usize,
) -> bool
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    tree_and_hyp_shows_states_are_apart_sunk_depth(tree, s_t, s_h, fsm, sink_output, depth)
        .is_some()
}

fn tree_and_hyp_shows_states_are_apart_sunk_depth<Tree>(
    tree: &Tree,
    s_t: Tree::S,
    s_h: State,
    fsm: &Mealy,
    sink_output: OutputSymbol,
    depth: usize,
) -> Option<Tree::S>
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    let mut queue = VecDeque::from([(s_t, s_h, 0)]);
    let inputs = toolbox::inputs_iterator(tree.input_size()).collect_vec();
    let step_tree = |q: &Tree::S, i| tree.get_out_succ(q.clone(), i);
    loop {
        let (q, r, d) = queue.pop_front()?;
        for &i in &inputs {
            // The O.T. must have the transition defined.
            if let Some((out_tree, d_t)) = step_tree(&q, i) {
                let (d_h, out_hyp) = fsm.step_from(r, i);
                if out_hyp == out_tree {
                    if out_tree == sink_output {
                        continue;
                    }
                    if d + 1 == depth {
                        continue;
                    }
                    queue.push_back((d_t, d_h, d + 1));
                } else {
                    return Some(d_t);
                }
            }
        }
    }
}

pub fn tree_and_hyp_states_apart_sunk<Tree>(
    tree: &Tree,
    s_t: Tree::S,
    s_h: State,
    fsm: &Mealy,
    sink_output: OutputSymbol,
) -> bool
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    tree_and_hyp_shows_states_are_apart_sunk(tree, s_t, s_h, fsm, sink_output).is_some()
}

fn tree_and_hyp_shows_states_are_apart_sunk<Tree>(
    tree: &Tree,
    s_t: Tree::S,
    s_h: State,
    fsm: &Mealy,
    sink_output: OutputSymbol,
) -> Option<Tree::S>
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    let mut queue = VecDeque::from([(s_t, s_h)]);
    let inputs = toolbox::inputs_iterator(tree.input_size()).collect_vec();
    let step_tree = |q: &Tree::S, i| tree.get_out_succ(q.clone(), i);
    loop {
        let (q, r) = queue.pop_front()?;
        for &i in &inputs {
            // The O.T. must have the transition defined.
            if let Some((out_tree, d_t)) = step_tree(&q, i) {
                let (d_h, out_hyp) = fsm.step_from(r, i);
                if out_hyp == out_tree {
                    if out_tree == sink_output {
                        continue;
                    }
                    queue.push_back((d_t, d_h));
                } else {
                    return Some(d_t);
                }
            }
        }
    }
}

/// Provide a witness if a tree and a hypothesis state is apart.
/// Note that this function is not equivalent to  
/// [`compute_witness`], as an observation tree is *partial*,
/// while FSMs are *complete*.
pub fn tree_and_hyp_compute_witness<Tree>(
    tree: &Tree,
    s_t: Tree::S,
    fsm: &Mealy,
    s_h: State,
) -> Option<Vec<InputSymbol>>
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    tree_and_hyp_shows_states_are_apart(tree, s_t.clone(), s_h, fsm)
        .map(|dest| tree.get_transfer_seq(dest, s_t))
}

/// This is a helper function for `tree_and_hyp_states_are_apart` and `compute_witness`.
/// *Always* put the tree state first!!
///
/// Given a pair of states, if the two states are apart, this function
/// returns `Some(fst)` where the transfer sequence from `tree_state` to
/// `fst` is the witness.
#[inline]
fn tree_and_hyp_shows_states_are_apart<Tree>(
    tree: &Tree,
    s_t: Tree::S,
    s_h: State,
    fsm: &Mealy,
) -> Option<Tree::S>
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    let mut queue = VecDeque::from([(s_t, s_h)]);
    let inputs = toolbox::inputs_iterator(tree.input_size()).collect_vec();
    let step_tree = |q: &Tree::S, i| tree.get_out_succ(q.clone(), i);
    loop {
        let (q, r) = queue.pop_front()?;
        for &i in &inputs {
            // The O.T. must have the transition defined.
            if let Some((out_tree, d_t)) = step_tree(&q, i) {
                let (d_h, out_hyp) = fsm.step_from(r, i);
                if out_hyp == out_tree {
                    queue.push_back((d_t, d_h));
                } else {
                    return Some(d_t);
                }
            }
        }
    }
}

/// This is a helper function for `states_are_apart` and `compute_witness`.
///
/// Given a pair of states, if the two states are apart, this function
/// returns `Some(fst)` where the transfer sequence from `first_state` to
/// `fst` is the witness.
#[allow(clippy::inline_always)]
#[inline(always)]
fn shows_states_are_apart<Tree, O>(tree: &Tree, s1: Tree::S, s2: Tree::S) -> Option<Tree::S>
where
    Tree: ObservationTree<InputSymbol, O>,
    O: PartialEq,
{
    let input_alphabet = toolbox::inputs_iterator(tree.input_size()).collect_vec();
    let mut work_list = VecDeque::from([(s1, s2)]);
    let step = |x, i| tree.get_out_succ(x, i);
    let tree_resp_pair_input = |x, y, i| Option::zip(step(x, i), step(y, i));
    loop {
        let (fst, snd) = work_list.pop_front()?; // If list is empty, states aren't apart.
        for (fst_o_d, snd_o_d) in input_alphabet
            .iter()
            .filter_map(|i| (tree_resp_pair_input(fst.clone(), snd.clone(), *i)))
        {
            let (fst_o, fst_d) = fst_o_d;
            let (snd_o, snd_d) = snd_o_d;
            if fst_o == snd_o {
                work_list.push_back((fst_d, snd_d));
            } else {
                return Some(fst_d);
            }
        }
    }
}
