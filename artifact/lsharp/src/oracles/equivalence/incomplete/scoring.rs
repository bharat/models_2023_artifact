use std::collections::HashMap;

use rustc_hash::{FxHashMap, FxHashSet};

use crate::definitions::mealy::{InputSymbol, Mealy};
use crate::definitions::FiniteStateMachine;

use super::tree::Node;

pub(super) fn score_sep(r: &Node, x: InputSymbol, fsm: &Mealy) -> usize {
    score(r, x, None, fsm)
}

pub(super) fn score_xfer(r: &Node, x: InputSymbol, r_x: &Node, fsm: &Mealy) -> usize {
    score(r, x, Some(r_x), fsm)
}

fn score<'a>(r: &Node, x: InputSymbol, r_x: Option<&'a Node>, fsm: &Mealy) -> usize {
    let w = {
        let mut ret = vec![x];
        if let Some(rx) = r_x {
            let seq: &[_] = rx.sep_seq.seq();
            assert!(!seq.is_empty(), "Sep Seq should not be empty.");
            ret.extend_from_slice(seq);
        }
        ret
    };
    let output_dest_map: FxHashMap<_, FxHashMap<_, FxHashSet<_>>> = r
        .label
        .iter()
        .map(|&s| (s, fsm.trace_from(s, &w)))
        .map(|(s, (d, o))| (s, o, d))
        .fold(HashMap::default(), |mut acc, (s, o, d)| {
            acc.entry(o).or_default().entry(d).or_default().insert(s);
            acc
        });
    if output_dest_map.len() == 1 {
        return usize::MAX;
    }
    let dest_overlap = !output_dest_map
        .values()
        .flat_map(HashMap::values)
        .all(|x| x.len() == 1);
    let is_valid_sep_seq = !dest_overlap && output_dest_map.len() > 1;
    if is_valid_sep_seq {
        return w.len();
    }
    let mut ret = 0;
    let mut states_in_non_inj_succs = 0;
    let mut num_valid_succs = 0;
    let mut num_succs = 0;
    let mut num_undist_states = 0;
    let n_r = r.label.len();
    let e = w.len();
    for (_resp, dest_src_map) in output_dest_map {
        let src_states: FxHashSet<_> = dest_src_map.values().flatten().copied().collect();
        let dest_states: FxHashSet<_> = dest_src_map.keys().copied().collect();
        if src_states.len() == dest_states.len() {
            num_valid_succs += 1;
        } else {
            states_in_non_inj_succs += src_states.len();
            num_undist_states += src_states.len() - dest_states.len();
        }
        num_succs += 1;
    }
    ret += states_in_non_inj_succs * n_r;
    ret -= num_valid_succs;
    ret *= n_r;
    ret -= num_succs;
    ret *= n_r;
    ret += num_undist_states;
    ret *= n_r;
    ret + e
}
