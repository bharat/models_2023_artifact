use std::collections::{BTreeMap, BTreeSet};

use itertools::Itertools;
use rayon::iter::IntoParallelIterator;
use rayon::prelude::ParallelIterator;
use rayon::slice::ParallelSliceMut;
use rustc_hash::FxHashSet;

use crate::definitions::characterisation::Characterisation;
use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;

use super::tree::SplittingTree;

pub struct HSIMethod;

/// Helper method for quickly getting the output word from a trace.
fn response_from(fsm: &Mealy, s: State, seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
    fsm.trace_from(s, seq).1
}

fn cons_sep_seq(fsm: &Mealy, s: State, block: &[State], tree: &SplittingTree) -> Vec<InputSymbol> {
    let mut ret = vec![];
    let mut block: FxHashSet<_> = block.iter().copied().collect();
    let mut s = s;
    while block.len() > 1 {
        let blk = block.iter().copied().collect_vec();
        let wit = tree.separating_sequence(&blk);
        assert!(
            wit.is_set(),
            "Could not find separating sequence for a block."
        );
        let wit = wit.seq();
        ret.extend_from_slice(wit);
        let (new_s, s_resp) = fsm.trace_from(s, wit);
        block = block
            .drain()
            .filter_map(|t| {
                let (t_p, t_resp) = fsm.trace_from(t, wit);
                (t_resp == s_resp).then_some(t_p)
            })
            .collect();
        s = new_s;
    }
    ret
}

fn construct_identifiers(
    fsm: &Mealy,
    s: State,
    splitting_tree: &SplittingTree,
) -> (State, BTreeSet<Vec<InputSymbol>>) {
    let mut hsi_local = BTreeSet::default();
    let mut block: Vec<_> = fsm.states().into_iter().collect();
    while block.len() > 1 {
        let w = cons_sep_seq(fsm, s, &block, splitting_tree);
        let s_resp = response_from(fsm, s, &w);
        block.retain(|t| response_from(fsm, *t, &w) == s_resp);
        hsi_local.insert(w);
    }
    (s, hsi_local)
}

/// Given the original characterisation map, minimise the harmonised state identifiers (remove
/// overlapping ones, for example).
fn minimise(
    fsm: &Mealy,
    idents: BTreeSet<Vec<InputSymbol>>,
    s: State,
) -> (State, BTreeSet<Vec<InputSymbol>>) {
    let mut other_states: FxHashSet<_> = fsm.states().into_iter().filter(|t| *t != s).collect();
    let local_identifiers = idents;
    let mut local_identifiers: Vec<_> = local_identifiers.iter().collect();
    local_identifiers.par_sort_by_key(|b| std::cmp::Reverse(b.len()));
    let mut new_local_idents = BTreeSet::default();
    for char_seq in local_identifiers {
        if other_states.is_empty() {
            break;
        }
        let s_resp = response_from(fsm, s, char_seq);
        other_states.retain(|t| response_from(fsm, *t, char_seq) == s_resp);
        new_local_idents.insert(char_seq.clone());
    }

    assert!(
        other_states.is_empty(),
        "Could not completely split {:?} from others.",
        s
    );
    (s, new_local_idents)
}

impl Characterisation for HSIMethod {
    /// Construct the characterisation map for the FSM using the HSI algorithm.
    fn characterisation_map(fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>> {
        let all_states = fsm.states();
        let stree = SplittingTree::new(fsm, &all_states);
        let local_idents = |s| construct_identifiers(fsm, s, &stree);
        let minimise_idents = |(s, idents)| minimise(fsm, idents, s);
        fsm.states()
            .into_par_iter()
            .map(local_idents)
            .map(minimise_idents)
            .collect()
    }
}
