use priority_queue::PriorityQueue;
use std::hash::Hash;

#[derive(Debug, Default)]
pub(super) struct PrioQueue<I: Hash + Eq, P: Ord> {
    /// First item is index of node, and the second is the priority.
    queue: PriorityQueue<I, P>,
}

impl<I: Hash + Eq, P: Ord + Copy + Default> PrioQueue<I, P> {
    /// Pop the highest priority element from the queue.
    pub fn pop(&mut self) -> Option<I> {
        self.queue.pop().map(|x| x.0)
    }

    /// Push an index (for a node), alongside the priority (i.e., the score.)
    pub fn push(&mut self, index: I, priority: P) {
        self.queue.push(index, priority);
    }

    /// Returns the max priority (which is the number of states in the node.)
    pub fn maximal(&self) -> P {
        // Get the highest priority index, or else return 0.
        // So as long as the highest priority is greater than or equal to
        // the number we want to compare, the if-cond will be false.
        self.queue.peek().map(|x| x.1).copied().unwrap_or_default()
    }
}
