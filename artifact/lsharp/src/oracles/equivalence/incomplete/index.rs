use derive_more::From;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Clone, Copy, From)]
pub(super) struct Index(usize);

impl Index {
    #[allow(clippy::inline_always)]
    #[inline(always)]
    pub fn to_index(self) -> usize {
        self.0
    }
}

impl From<Index> for usize {
    fn from(idx: Index) -> Self {
        idx.to_index()
    }
}
