use fnv::FnvHashMap;
use std::{fmt::Debug, hash::Hash};

/// Helper struct to hold all the state pairs.
/// `K` is the type of the states of the FSM.
#[derive(Debug, Default, Clone)]
pub(super) struct SeparatingNodes<K, V> {
    /// Each pair of states maps to the node index which separates the pair.
    inner: FnvHashMap<(K, K), V>,
}

impl<K, V> SeparatingNodes<K, V>
where
    K: PartialOrd,
{
    fn make_key(x: K, y: K) -> (K, K) {
        if x < y {
            (x, y)
        } else {
            (y, x)
        }
    }
}

impl<K, V> SeparatingNodes<K, V>
where
    K: PartialEq + Hash + PartialOrd + Eq,
    V: Copy,
{
    pub fn insert_pair(&mut self, s1: K, s2: K, idx: V) {
        let key = Self::make_key(s1, s2);
        self.inner.insert(key, idx);
    }

    /// Returns ``Some(Index)`` of the LCA for a pair of states, if present.
    pub fn check_pair(&self, s1: K, s2: K) -> Option<V> {
        let key = Self::make_key(s1, s2);
        self.inner.get(&key).copied()
    }
}
