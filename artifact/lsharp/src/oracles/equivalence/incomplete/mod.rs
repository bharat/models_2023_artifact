pub mod hads;
pub mod hsi;
pub mod iads;
pub mod impl_ads;
pub mod logged_iads;
pub mod tree;

// Private modules we use to construct and use the splitting tree.
mod best_node;
mod index;
mod prio_queue;
mod scoring;
mod sep_seq;
mod separating_nodes;
mod splits;
