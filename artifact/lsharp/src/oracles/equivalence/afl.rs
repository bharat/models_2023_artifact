use fnv::FnvHashMap;
use itertools::Itertools;

use std::ffi::OsString;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::process::Command;
use std::{cell::RefCell, rc::Rc, thread, time::Duration};

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};
use crate::definitions::FiniteStateMachine;
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::membership::Oracle as OQOracle;

use super::{CounterExample, EquivalenceOracle};
/// AFL++ based external equivalence oracle.
///
/// This module is not the implementation of the equivalence oracle, it is
/// just a wrapper written to communicate with the actual oracle.
pub struct ExtAFLOracle<'a, T> {
    oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    extra_states: usize,
}

impl<'a, T> Drop for ExtAFLOracle<'a, T> {
    fn drop(&mut self) {
        let _kill_afl_cmd = Command::new("killall")
            .arg("afl-fuzz")
            .spawn()
            .expect("AFL could not be exited.");
    }
}

impl<'a, T> EquivalenceOracle<'a, T> for ExtAFLOracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        {
            let sleepy = Duration::from_millis(100);
            thread::sleep(sleepy);
        }
        self.find_counterexample_with_lookahead(hypothesis, self.extra_states)
    }
    fn find_counterexample_with_lookahead(
        &mut self,
        hypothesis: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        let path_to_res_dir =
            Path::new("/home/bharat/rust/automata-lib/sul/res_dir/fuzzerM/crashes");

        log::info!("Reading tests from {}", path_to_res_dir.to_string_lossy());

        let test_suite = read_all_mutations(
            Path::new("/home/bharat/rust/automata-lib"),
            self.oq_oracle.borrow().borrow_tree().input_size(),
        );
        // let test_suite = read_all_tests(
        //     path_to_res_dir,
        //     self.oq_oracle.borrow().borrow_tree().input_size(),
        // );

        log::info!("Read {} tests from AFL", test_suite.len());
        for input_vec in test_suite {
            let hyp_output = hypothesis.trace(&input_vec).1.to_vec();
            let sut_output = RefCell::borrow_mut(&self.oq_oracle).output_query(&input_vec);
            if sut_output.len() < hyp_output.len() {
                let hyp_out_lim = &sut_output[..sut_output.len()];
                if hyp_out_lim != sut_output {
                    let ce_input = input_vec[..sut_output.len()].to_vec();
                    let ce_output = sut_output;
                    let ret = Some((ce_input, ce_output));
                    log::info!("Found CE AFL");
                    return ret;
                }
            } else if hyp_output != sut_output {
                let ce_input = input_vec;
                let ce_output = sut_output;
                let ret = Some((ce_input, ce_output));
                log::info!("Found CE AFL");
                return ret;
            }
        }
        log::info!("Did not find any CEs with AFL");
        None
    }
}

impl<'a, T> ExtAFLOracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync,
{
    pub fn new(
        oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
        _location: String,
        extra_states: usize,
        _input_map: FnvHashMap<InputSymbol, String>,
        _output_map: FnvHashMap<OutputSymbol, String>,
    ) -> Self {
        Self {
            oq_oracle,
            extra_states,
        }
    }
}

fn read_all_mutations(mutations_file_path: &Path, input_size: usize) -> Vec<Vec<InputSymbol>> {
    let mut ret = vec![];
    let all_files = std::fs::read_dir(mutations_file_path).expect("Could not read AFL directory!");
    log::info!("Found tests dir!");
    let all_files = all_files
        .into_iter()
        .filter_map(std::result::Result::ok)
        .map(|x| x.path())
        .filter(|x| x.is_file())
        .filter(|x| x.extension() == Some(&OsString::from("txt")))
        .collect_vec();
    for file_path in all_files {
        let file = File::open(file_path.clone()).unwrap_or_else(|_| {
            log::info!("Cannot open test file!");
            panic!("Cannot read test file, exiting.")
        });
        let buf = BufReader::new(file);
        let mut seq = vec![];
        for line in buf.lines().map(std::result::Result::unwrap) {
            if let Ok(symbol) = line.parse::<u16>() {
                if usize::from(symbol) >= input_size {
                    ret.push(seq.clone());
                    seq.clear();
                } else {
                    let i = InputSymbol::from(symbol);
                    seq.push(i);
                }
            } else {
                ret.push(seq.clone());
                seq.clear();
            }
        }
    }
    ret
}

fn read_all_tests(path_to_res_dir: &Path, input_size: usize) -> Vec<Vec<InputSymbol>> {
    let mut ret = vec![];
    let all_files = std::fs::read_dir(path_to_res_dir).expect("Could not read AFL directory!");
    log::info!("Found tests dir!");
    let all_files = all_files
        .into_iter()
        .filter_map(std::result::Result::ok)
        .map(|x| x.path())
        .collect_vec();
    log::info!("Number of tests by AFL: {}", all_files.len());
    if all_files.len() == 10001 {
        Command::new("killall")
            .arg("afl-fuzz")
            .spawn()
            .expect("AFL could not be exited.");
    }
    let max_input = u16::try_from(input_size - 1).expect("Should be safe!");
    'test_file: for file_path in all_files {
        let file = File::open(file_path.clone()).unwrap_or_else(|_| {
            log::info!("Cannot open test file!");
            panic!("Cannot read test file, exiting.")
        });
        let buf = BufReader::new(file);
        let input_seq: Result<Vec<String>, _> = buf.lines().try_collect();
        let Ok(input_seq) = input_seq else {
            continue;
        };
        let input_seq = input_seq.into_iter().join(" ");

        let mut test_seq = vec![];
        for input in input_seq.split_whitespace() {
            if let Ok(i) = input.parse::<u16>() {
                if i > max_input {
                    continue;
                }
                let i = InputSymbol::from(i);
                test_seq.push(i);
            } else {
                continue 'test_file;
            }
        }

        ret.push(test_seq);
    }
    log::info!("Finished reading AFL tests.");
    // assert!(!ret.is_empty());

    ret
}
