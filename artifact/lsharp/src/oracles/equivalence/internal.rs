use std::{cell::RefCell, rc::Rc};

use crate::definitions::mealy::{shortest_separating_sequence, Mealy};
use crate::oracles::membership::Oracle as OQOracle;
use crate::{
    definitions::mealy::{InputSymbol, OutputSymbol},
    learner::obs_tree::ObservationTree,
};

use super::EquivalenceOracle;

/// Generic factory Equivalence oracle.
///
/// This EO supports the W, Wp, HSI, and HADS oracles.
#[allow(clippy::module_name_repetitions)]
pub struct PerfectEO<'a, T> {
    _oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    fsm: Mealy,
}

impl<'a, T> PerfectEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn new(oq_oracle: Rc<RefCell<OQOracle<'a, T>>>, fsm: Mealy) -> Self {
        Self {
            _oq_oracle: oq_oracle,
            fsm,
        }
    }
}

impl<'a, T> EquivalenceOracle<'a, T> for PerfectEO<'a, T> {
    fn get_counts(&mut self) -> (usize, usize) {
        (0, 0)
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> super::CounterExample {
        shortest_separating_sequence(&self.fsm, hypothesis, None, None)
    }

    fn find_counterexample_with_lookahead(
        &mut self,
        hyp: &Mealy,
        _lookahead: usize,
    ) -> super::CounterExample {
        self.find_counterexample(hyp)
    }
}
