use std::{collections::HashMap, rc::Rc};

use fnv::FnvHashMap;
use itertools::Itertools;
use rayon::prelude::{IntoParallelIterator, ParallelIterator};

use crate::ads::{AdaptiveDistinguishingSequence, AdsStatus};
use crate::definitions::mealy::{InputSymbol, OutputSymbol};
use crate::learner::obs_tree::ObservationTree;
use crate::util::toolbox;

use super::utils::{compute_reg_score, partition_on_output};

#[derive(Debug, Default, Clone, derive_more::Constructor)]
struct Node {
    input: Option<InputSymbol>,
    children: FnvHashMap<OutputSymbol, Node>,
    score: usize,
}

#[derive(Debug, Clone)]
pub struct Ads {
    curr_node: Rc<Node>,
    initial_node: Rc<Node>,
}

impl Node {
    #[must_use]
    fn leaf() -> Self {
        Self::default()
    }
    #[must_use]
    fn get_score(&self) -> usize {
        self.score
    }
    #[must_use]
    fn get_input(&self) -> Option<InputSymbol> {
        self.input
    }
    #[must_use]
    fn get_child_node(&self, last_output: OutputSymbol) -> Option<&Node> {
        self.children.get(&last_output)
    }
}

impl Ads {
    #[must_use]
    pub fn new<Tree>(
        o_tree: &Tree,
        current_block: &[Tree::S],
        sink_out: Option<OutputSymbol>,
    ) -> Self
    where
        Tree: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync,
    {
        let initial_node = Self::construct_ads(o_tree, current_block, sink_out.expect("Safe"));
        let ini = Rc::new(initial_node);
        let curr = Rc::clone(&ini);
        Self {
            initial_node: ini,
            curr_node: curr,
        }
    }

    #[must_use]
    pub fn get_score(&self) -> usize {
        self.initial_node.get_score()
    }

    #[allow(clippy::too_many_lines)]
    #[must_use]
    fn construct_ads<Tree>(o_tree: &Tree, current_block: &[Tree::S], sink_out: OutputSymbol) -> Node
    where
        Tree: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync,
    {
        let block_size = current_block.len();

        // If we only have one state, there's no point in distinguishing anything.
        if block_size == 1 {
            return Node::leaf();
        }
        // input -> (base,max_rec) scores
        let mut split_score = FnvHashMap::<InputSymbol, (usize, usize)>::default();
        let input_size = o_tree.input_size();
        let input_alphabet = toolbox::inputs_iterator(input_size).collect_vec();
        let (max_input, _max_base) =
            maximal_base_input(input_alphabet, o_tree, current_block, &mut split_score);

        let make_subtree = |u_i, o, o_part: Vec<_>| {
            let u_i_o = o_part.len();
            let child_score = if o == sink_out {
                0
            } else {
                let i_subtree = Self::construct_ads(o_tree, &o_part, sink_out);
                i_subtree.get_score()
            };
            compute_reg_score(u_i_o, u_i, child_score)
        };
        let max_rec = {
            // Get the outputs and the partitions for input i.
            let o_partitions = partition_on_output(o_tree, current_block, max_input);
            // Compute the sub-trees for the partitions.
            let u_i: usize = o_partitions.values().map(Vec::len).sum();
            let i_score: usize = o_partitions
                .into_iter()
                .map(|(o, o_part)| make_subtree(u_i, o, o_part))
                .sum();
            i_score
        };
        let max_input_score = max_rec;
        // Mistake : max_rec already contains the max_base score.
        // let max_input_score = max_base + max_rec;
        let inputs_to_keep: Vec<InputSymbol> = split_score
            .into_par_iter()
            .filter(|(_, (base, rec))| (*base + *rec) >= max_input_score)
            .map(|(i, _)| i)
            .collect();

        assert!(
            !inputs_to_keep.is_empty(),
            "No input available during ADS comp."
        );

        let subtree_info = inputs_to_keep
            .into_par_iter()
            .map(|i| {
                // Get the outputs and the partitions for input i.
                let o_partitions = partition_on_output(o_tree, current_block, i);
                // Compute the sub-trees for the partitions.
                let u_i: usize = o_partitions.values().map(Vec::len).sum();
                let (o_scores, data): (Vec<usize>, Vec<_>) = o_partitions
                    .into_iter()
                    .map(|(o, o_part)| Self::compute_o_subtree(o_tree, o, &o_part, sink_out, u_i))
                    .unzip();
                let i_score: usize = o_scores.into_iter().sum();
                (i, i_score, data)
            })
            .filter(|(_, i_score, _)| *i_score >= max_input_score)
            .max_by(|(_, i_score_a, _), (_, i_score_b, _)| i_score_a.cmp(i_score_b))
            .map(|(i, i_score, i_children)| {
                let children: HashMap<OutputSymbol, Node, _> = i_children.into_iter().collect();
                Node::new(Some(i), children, i_score)
            });

        subtree_info.expect("Safe")
    }

    /// Given an output and the partition for the same,
    /// this function returns the ADS of the partition and its score.
    /// ## Note
    /// We special-case the `sink_out` symbol: if `o == sink_out`, we
    /// simply return a leaf node with a score of 0 (as we know that
    /// there are only self-loops after a sink state).
    fn compute_o_subtree<Tree>(
        o_tree: &Tree,
        o: OutputSymbol,
        o_part: &[Tree::S],
        sink_out: OutputSymbol,
        u_i: usize,
    ) -> (usize, (OutputSymbol, Node))
    where
        Tree: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync,
    {
        let o_subtree = {
            if o == sink_out {
                Node::new(None, HashMap::default(), 0)
            } else {
                Self::construct_ads(o_tree, o_part, sink_out)
            }
        };
        let o_child_score = o_subtree.get_score();
        let u_i_o = o_part.len();
        let o_child = (o, o_subtree);
        let o_score = compute_reg_score(u_i_o, u_i, o_child_score);
        (o_score, o_child)
    }
}

fn maximal_base_input<Tree>(
    input_alphabet: Vec<InputSymbol>,
    o_tree: &Tree,
    current_block: &[Tree::S],
    split_score: &mut FnvHashMap<InputSymbol, (usize, usize)>,
) -> (InputSymbol, usize)
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    let mut ret_input = InputSymbol::new(0);
    let mut ret_pairs = 0;
    for i in input_alphabet {
        let (state_nums, max_rec): (Vec<_>, usize) = partition_on_output(o_tree, current_block, i)
            .into_values()
            .map(|next_states| next_states.len())
            .fold((Vec::new(), 0), |(mut state_nums, rec_score), u_i_o| {
                state_nums.push(u_i_o);
                (state_nums, rec_score + (u_i_o * (u_i_o - 1)))
            });
        let u_i: usize = state_nums.iter().copied().sum();
        let num_apart_pairs: usize = state_nums
            .into_iter()
            .map(|u_i_o| u_i_o * (u_i - u_i_o))
            .sum();
        split_score.insert(i, (num_apart_pairs, max_rec));
        if num_apart_pairs > ret_pairs {
            ret_input = i;
            ret_pairs = num_apart_pairs;
        }
    }
    (ret_input, ret_pairs)
}

impl AdaptiveDistinguishingSequence for Ads {
    fn next_input(&mut self, prev_output: Option<OutputSymbol>) -> Result<InputSymbol, AdsStatus> {
        let out_child = |output| {
            let child = self.curr_node.get_child_node(output);
            child
                .map(|x| Rc::new(x.clone()))
                .ok_or(AdsStatus::Unexpected)
        };
        let root = || Ok(Rc::clone(&self.curr_node));
        self.curr_node = prev_output.map_or_else(root, out_child)?;
        self.curr_node.get_input().ok_or(AdsStatus::Done)
    }

    fn get_print_tree(&self) -> Box<[u8]> {
        todo!()
    }

    #[allow(clippy::cast_precision_loss)]
    fn identification_power(&self) -> f32 {
        self.get_score() as f32
    }

    fn reset_to_root(&mut self) {
        self.curr_node = Rc::clone(&self.initial_node);
    }
}

#[cfg(test)]
mod tests {
    use fnv::FnvBuildHasher;

    use crate::{
        definitions::mealy::{InputSymbol, OutputSymbol, State},
        learner::obs_tree::normal::MapObsTree as ObsTree,
        learner::obs_tree::ObservationTree,
    };

    use super::Ads;

    /// We should be able to construct the ADS.
    #[test]
    fn ads_regression_1() {
        let i_str = "[[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[2,3,8,8,8,1,12,7,4,7,5,0,2,7,6,6,6,8,3,12,11,8,8,7,3,2]]";
        let o_str = "[[0],[0],[0],[0],[0],[1],[0],[1],[0],[2],[1], [2], [1], [0,3,3,3,3,3,1,1,3,1,1,3,3,1,3,4,4,3,3,3,3,3,3,3,3,3]]";

        let mut o_tree = ObsTree::<FnvBuildHasher>::new(13);

        let input_seqs: Vec<Vec<InputSymbol>> = serde_json::from_str(i_str).expect("Safe");
        let output_seqs: Vec<Vec<OutputSymbol>> = serde_json::from_str(o_str).expect("Safe");
        for (seq, out) in Iterator::zip(input_seqs.into_iter(), output_seqs.into_iter()) {
            o_tree.insert_observation(None, &seq, &out);
        }

        let block = [State::new(0), State::new(3)];
        let _x = Ads::new(&o_tree, &block, Some(OutputSymbol::new(u16::MAX)));
    }
}
