use super::{
    super::{AdaptiveDistinguishingSequence, AdsStatus},
    splitting_tree::{self, find_lca, SplittingNode},
};
use crate::{
    definitions::{
        mealy::{InputSymbol, Mealy, OutputSymbol, State},
        FiniteStateMachine,
    },
    util::data_structs::arena_tree::ArenaTree,
};
use fnv::FnvHashMap;
use std::collections::VecDeque;

#[derive(PartialEq, Eq, Debug, Default)]
struct AdsNode {
    /// The input to use for this node.
    input: Option<InputSymbol>,
    /// Initial states for this node.
    initial_states: Box<[State]>,
    /// received output -> index of next node
    children: FnvHashMap<OutputSymbol, usize>,
}

#[derive(Debug)]
pub struct AdsTree {
    ads: ArenaTree<AdsNode, ()>,
    curr: usize,
}

impl AdaptiveDistinguishingSequence for AdsTree {
    fn next_input(&mut self, prev_output: Option<OutputSymbol>) -> Result<InputSymbol, AdsStatus> {
        // What if the below unwrap is a None?
        // Should never happen, as long as we do not run the
        // ADS on a single-state hypothesis.
        prev_output.map_or(
            Ok(self.ads.arena[self.curr].val.input.unwrap()),
            |prev_output| {
                if self.ads.arena[self.curr].val.children.is_empty() {
                    return Err(AdsStatus::Done);
                }
                let child_idx = self.ads.arena[self.curr]
                    .val
                    .children
                    .get(&prev_output)
                    .copied();
                child_idx.map_or(Err(AdsStatus::Unexpected), |child_idx| {
                    self.curr = child_idx;
                    let x = self.curr;
                    self.ads.arena[x].val.input.ok_or(AdsStatus::Done)
                })
            },
        )
    }
    fn identification_power(&self) -> f32 {
        0.0
    }

    fn reset_to_root(&mut self) {
        self.curr = 0;
    }

    fn get_print_tree(&self) -> Box<[u8]> {
        todo!()
    }
}
impl AdsTree {
    #[must_use]
    pub fn new(hyp: &Mealy) -> Self {
        let s_tree = splitting_tree::new(hyp);
        let ads = make_ads(&s_tree, hyp);
        Self { ads, curr: 0 }
    }
    #[allow(dead_code)]
    #[must_use]
    pub fn get_initial_states(&self) -> Box<[State]> {
        self.ads.arena[self.curr].val.initial_states.clone()
    }

    #[must_use]
    #[allow(dead_code)]
    fn get_leaves_indices(&self) -> Vec<usize> {
        (0..self.ads.size())
            .into_iter()
            .filter(|x| self.ads.arena[*x].val.children.is_empty())
            .collect()
    }
}
impl AdsNode {
    fn update_children(&mut self, output: OutputSymbol, child_idx: usize) {
        self.children.insert(output, child_idx);
    }
    fn set_separator(&mut self, sep: InputSymbol) {
        self.input = Some(sep);
    }
}

/// Returns an ADS, modified for the cases where the Splitting Tree might be incomplete.
#[must_use]
fn make_ads(tree: &ArenaTree<SplittingNode, ()>, hyp: &Mealy) -> ArenaTree<AdsNode, ()> {
    let hyp_states: Vec<_> = hyp.states().into_iter().collect(); // initial
    let root_node = AdsNode {
        initial_states: hyp_states.clone().into_boxed_slice(),
        ..Default::default()
    };
    let mut ads = ArenaTree::default();
    let root_idx = ads.node(root_node);

    // Next node index, initial states block, and current states block.
    let mut work_list: VecDeque<(usize, Vec<State>, Vec<State>)> = VecDeque::new();
    work_list.push_back((
        root_idx,
        hyp_states.clone(), // initial
        hyp_states,         // current
    ));
    while !work_list.is_empty() {
        let (mut curr_idx, initial_block, mut curr_block) = work_list.pop_front().expect("Safe");

        // If the set of initial states is 1, we have reached a leaf.
        if initial_block.len() < 2 {
            continue;
        }
        // Find the LCA of the current block.
        let lca_tree_idx = find_lca(tree, &curr_block);

        // Once we have the LCA, apply the separator of the
        // node to the current set of the states and add the children
        // to the work list.
        // NOTE: If the separator does not exist, then simply mark the
        // current states in the
        // current node and move on to the next item in the work list.
        if let Some(separator) = tree.arena[lca_tree_idx].val.get_separator() {
            // If the separator is of length 1, it indicates that the symbol will
            // partition the current block.
            if separator.len() == 1 {
                ads.arena[curr_idx].val.set_separator(separator[0]);
                refine_block(
                    &mut ads,
                    hyp,
                    separator[0],
                    &initial_block,
                    &curr_block,
                    curr_idx,
                )
                .into_iter()
                .for_each(|x| work_list.push_back(x));
            } else {
                let max_cnt = separator.len() - 1;
                for (cnt, symbol) in separator.iter().enumerate() {
                    // if the count is max, then we have the block refining to do
                    // otherwise, we will simply make the intermediate nodes.
                    // We will first do the looping and construction of the
                    // intermediate nodes.
                    if cnt == max_cnt {
                        refine_block(
                            &mut ads,
                            hyp,
                            *symbol,
                            &initial_block,
                            &curr_block,
                            curr_idx,
                        )
                        .into_iter()
                        .for_each(|x| work_list.push_back(x));
                    } else {
                        let mut new_current_block = vec![];
                        let mut new_output_block = vec![];
                        for (state, output) in curr_block.iter().map(|s| hyp.step_from(*s, *symbol))
                        {
                            new_current_block.push(state);
                            new_output_block.push(output);
                        }
                        // Make intermediate child node.
                        let child_node = AdsNode {
                            input: Some(*symbol),
                            initial_states: initial_block.clone().into_boxed_slice(),
                            children: FnvHashMap::default(),
                        };
                        let child_idx = ads.node(child_node);
                        ads.arena[curr_idx]
                            .val
                            .children
                            .insert(new_output_block[0], child_idx);
                        curr_idx = child_idx;
                        // Update the current states.
                        curr_block = new_current_block;
                    }
                }
            }
        } else {
            let new_node = AdsNode {
                initial_states: initial_block.into_boxed_slice(),
                ..Default::default()
            };
            ads.arena[curr_idx].update(new_node);
        }
    }
    ads
}

/// Refine a block of states using the symbol `separator`
/// and return the data for the children to be put into the queue.
fn refine_block(
    ads: &mut ArenaTree<AdsNode, ()>,
    hyp: &Mealy,
    separator: InputSymbol,
    initial_block: &[State],
    curr_block: &[State],
    curr_idx: usize,
) -> Vec<(usize, Vec<State>, Vec<State>)> {
    let mut output_map_split = FnvHashMap::default();
    curr_block
        .iter()
        .map(|s| hyp.step_from(*s, separator))
        .zip(initial_block.iter().copied())
        .for_each(|((dest, output), initial)| {
            output_map_split
                .entry(output)
                .or_insert_with(Vec::new)
                .push((initial, dest));
        });
    // Now, for each of the outputs, construct a child node
    // and push the details into the work_list for further processing.
    output_map_split
        .into_iter()
        .map(|(output, initial_dest_vec)| {
            let (dest_block, child_initial_block) = initial_dest_vec.into_iter().fold(
                (Vec::new(), Vec::new()),
                |(mut dest_block, mut child_initial_block), (i, d)| {
                    dest_block.push(d);
                    child_initial_block.push(i);
                    (dest_block, child_initial_block)
                },
            );
            // let dest_block: Vec<_> = initial_dest_vec.iter().map(|(_i, d)| *d).collect();
            // let child_initial_block: Vec<_> = initial_dest_vec.iter().map(|(i, _d)| *i).collect();
            let child_node = AdsNode {
                input: None,
                initial_states: child_initial_block.clone().into_boxed_slice(),
                children: FnvHashMap::default(),
            };
            let child_idx = ads.node(child_node);
            ads.arena[curr_idx].val.update_children(output, child_idx);
            (child_idx, child_initial_block, dest_block)
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use fnv::FnvHashSet;

    use super::super::adaptive_distinguishing_seq;
    use super::super::adaptive_distinguishing_seq::State;
    use crate::definitions::FiniteStateMachine;
    use crate::util::parsers::machine::read_mealy_from_file;
    use std::collections::hash_map::RandomState;
    use std::path::Path;

    impl adaptive_distinguishing_seq::AdsTree {
        fn leaves_states(&self) -> FnvHashSet<State> {
            let mut ret = FnvHashSet::default();
            (0..self.ads.size())
                .into_iter()
                .filter(|n| self.ads.arena[*n].val.children.is_empty())
                .map(|x| self.ads.arena[x].val.initial_states.clone())
                .for_each(|x| {
                    for y in x.iter() {
                        ret.insert(*y);
                    }
                });
            ret
        }

        #[allow(dead_code)]
        fn num_leaves(&self) -> usize {
            (0..self.ads.size())
                .into_iter()
                .filter(|n| self.ads.arena[*n].val.children.is_empty())
                .count()
        }
        fn are_leaves_singletons(&self) -> bool {
            (0..self.ads.size())
                .into_iter()
                .filter(|n| self.ads.arena[*n].val.children.is_empty())
                .all(|n| self.ads.arena[n].val.initial_states.len() == 1)
        }
    }

    #[test]
    fn ads_is_complete() {
        let file_name = Path::new("example.dot");
        let (fsm, _, _) = read_mealy_from_file(file_name.to_str().unwrap());
        let ads = adaptive_distinguishing_seq::AdsTree::new(&fsm);
        let states_set_from_ads = ads.leaves_states();
        assert!(ads.are_leaves_singletons());
        assert_eq!(states_set_from_ads.len(), fsm.states().len());
    }
}
