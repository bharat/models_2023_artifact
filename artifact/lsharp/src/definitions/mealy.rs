use std::{collections::VecDeque, fmt};

use datasize::DataSize;
use derive_alias::derive_alias;
use derive_more::{Constructor, From};
use rayon::prelude::{IntoParallelRefIterator, ParallelIterator};
use rustc_hash::{FxHashMap, FxHashSet};
use serde::{Deserialize, Serialize};

use crate::oracles::equivalence::CounterExample;

use self::access_mod::AccessTree;

use super::FiniteStateMachine;

derive_alias! {
    derive_basic_wrapper => #[derive(Clone, Default, Copy, Debug, Eq, Hash, PartialEq, PartialOrd, Ord, Deserialize, Serialize)]
}

derive_basic_wrapper! {
/// Newtype for an input symbol: wraps a u16.
#[derive(DataSize)]
#[derive(From, Constructor)]
pub struct InputSymbol(u16);
}

derive_basic_wrapper! {
/// Newtype for an output symbol: wraps a u16.
#[derive(DataSize)]
#[derive(From, Constructor)]
pub struct OutputSymbol(u16);
}

derive_basic_wrapper! {
/// Newtype for a state: wraps a u32.
#[derive(DataSize)]
#[derive(From, Constructor)]
pub struct State(u32);
}

impl fmt::Display for InputSymbol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "I{}", self.0)
    }
}

impl fmt::Display for OutputSymbol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "O{}", self.0)
    }
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "S{}", self.0)
    }
}

impl State {
    #[inline]
    #[must_use]
    pub fn raw(self) -> u32 {
        self.0
    }
}

impl InputSymbol {
    #[inline]
    #[must_use]
    pub fn raw(&self) -> u16 {
        self.0
    }
}

impl From<usize> for InputSymbol {
    #[inline]
    fn from(a: usize) -> Self {
        Self::new(a.try_into().expect("Safe"))
    }
}

impl From<InputSymbol> for usize {
    #[inline]
    fn from(a: InputSymbol) -> usize {
        a.0 as usize
    }
}

impl OutputSymbol {
    #[inline]
    #[must_use]
    pub fn raw(self) -> u16 {
        self.0
    }
}

#[derive(Debug, Clone, PartialEq, Eq, DataSize, Constructor)]
pub struct Mealy {
    /// States of the Mealy machine.
    states: Vec<State>,
    /// Initial state, usually State(0).
    initial_state: State,
    /// Set of `InputSymbol`.
    input_alphabet: Vec<InputSymbol>,
    /// Set of `OutputSymbol`.
    output_alphabet: FxHashSet<OutputSymbol>,
    /// Mealy function
    trans_function: FxHashMap<(State, InputSymbol), (State, OutputSymbol)>,
}

impl FiniteStateMachine for Mealy {
    fn states(&self) -> Vec<State> {
        self.states.clone()
    }

    fn is_complete(&self) -> bool {
        self.states.par_iter().all(|&s| {
            self.input_alphabet.par_iter().all(|&i| {
                let tr = &(s, i);
                self.trans_function.get(tr).map_or(false, |(d, o)| {
                    self.states.contains(d) && self.output_alphabet.contains(o)
                })
            })
        })
    }

    fn initial_state(&self) -> State {
        self.initial_state
    }

    fn step_from(&self, src: State, i: InputSymbol) -> (State, OutputSymbol) {
        let trans = &(src, i);
        *self
            .trans_function
            .get(trans)
            .expect("FSM is not completely defined.")
    }

    fn trace_from(&self, src: State, input_word: &[InputSymbol]) -> (State, Box<[OutputSymbol]>) {
        let mut curr = src;
        let mut output = Vec::with_capacity(input_word.len());
        for i in input_word {
            let (dest, o) = self.step_from(curr, *i);
            curr = dest;
            output.push(o);
        }
        (curr, output.into_boxed_slice())
    }

    fn get_dest<T: IntoIterator<Item = InputSymbol>>(&self, src: State, input_word: T) -> State {
        input_word
            .into_iter()
            .fold(src, |s, i| self.step_from(s, i).0)
    }

    fn trace(&self, input_word: &[InputSymbol]) -> (State, Box<[OutputSymbol]>) {
        self.trace_from(self.initial_state, input_word)
    }

    fn input_alphabet(&self) -> Vec<InputSymbol> {
        self.input_alphabet.clone()
    }

    fn output_alphabet(&self) -> FxHashSet<OutputSymbol> {
        self.output_alphabet.clone()
    }
}

/// Find the shortest separating sequence between `reference` and `other`.
///
/// If an initial state is `None`, we take the actual initial state of the machine.
/// If an initial state is `State(x)`, we use it as initial.
pub fn shortest_separating_sequence<M, St>(
    reference: &M,
    other: &M,
    ref_initial: St,
    other_initial: St,
) -> CounterExample
where
    M: FiniteStateMachine + Send + Sync,
    St: Into<Option<State>>,
{
    let ref_initial_state = ref_initial
        .into()
        .unwrap_or_else(|| reference.initial_state());
    let hyp_initial_state = other_initial
        .into()
        .unwrap_or_else(|| other.initial_state());
    let input_alphabet = reference.input_alphabet();
    let mut visited_pairs = FxHashSet::default();
    let mut access_tree = AccessTree::new();
    let init_idx = 0;

    let initial_pair = (ref_initial_state, hyp_initial_state, init_idx);
    let mut work_list = VecDeque::from([initial_pair]);
    while let Some((ref_state, hyp_state, idx)) = work_list.pop_front() {
        for i in &input_alphabet {
            let (ref_dest, ref_out) = reference.step_from(ref_state, *i);
            let (hyp_dest, hyp_out) = other.step_from(hyp_state, *i);

            // If the outputs do not match, we have our CE.
            // Return the current access seq + the input symbol
            // Also the expected (i.e., reference) output.
            if ref_out != hyp_out {
                let mut input_seq = access_tree.access_seq(idx);
                input_seq.push(*i);
                let (_, output_seq) = reference.trace(&input_seq);
                return Some((input_seq, output_seq.to_vec()));
            }

            if (ref_dest, hyp_dest) == (ref_state, hyp_state) {
                continue;
            }

            // If the outputs match, add the destination pairs,
            // unless we've seen the destination pairs already or
            // the dest. pair is the current pair (aka a loop).
            if !visited_pairs.contains(&(ref_dest, hyp_dest)) {
                let dest_idx = access_tree.insert_child_at(idx, *i);
                work_list.push_back((ref_dest, hyp_dest, dest_idx));
            }
            visited_pairs.insert((ref_state, hyp_state));
        }
    }
    None
}

mod access_mod {
    use rustc_hash::FxHashMap;

    use crate::util::data_structs::arena_tree::ArenaTree;

    use super::InputSymbol;

    #[derive(Debug, Default)]
    struct AccessNode {
        // An InputSymbol and its corresponding child node.
        children: FxHashMap<InputSymbol, usize>,
    }

    /// Prefix tree for storing access sequences.
    pub(super) struct AccessTree {
        tree: ArenaTree<AccessNode, InputSymbol>,
    }

    impl AccessTree {
        pub(super) fn new() -> Self {
            let root = AccessNode::default();
            let mut tree = ArenaTree::default();
            assert_eq!(tree.node(root), 0);
            Self { tree }
        }

        pub(super) fn insert_child_at(&mut self, curr: usize, i: InputSymbol) -> usize {
            let c_node = AccessNode::default();
            let c_idx = self.tree.node_with_parent(c_node, curr, i);
            let curr_node = &mut self.tree.arena[curr].val.children;
            curr_node.insert(i, c_idx);
            c_idx
        }

        pub(super) fn access_seq(&self, to_state: usize) -> Vec<InputSymbol> {
            let from_state = 0;
            let mut access_seq = Vec::new();
            if to_state == from_state {
                return access_seq;
            }
            let dest_parent_idx = from_state;
            let mut curr_state = to_state;
            loop {
                let (i, parent_idx) = self.tree.arena[curr_state].parent.unwrap();
                access_seq.push(i);
                if parent_idx == dest_parent_idx {
                    break;
                }
                curr_state = parent_idx;
            }
            access_seq.reverse();
            access_seq
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{FiniteStateMachine, Mealy, State};
    use crate::util::parsers::machine::read_mealy_from_file;
    use rstest::rstest;
    use std::path::Path;

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    /// Ensure that the initial state is set.
    #[rstest]
    #[case("tests/src_models/trial.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    fn fsm_initial_state_is_set(#[case] file_name: &str) {
        let fsm = load_basic_fsm(file_name);
        assert_ne!(fsm.initial_state(), State::new(u32::MAX));
    }

    /// Ensure that the set of states is set.
    #[test]
    fn fsm_states_loaded_correctly() {
        let fsm = load_basic_fsm("tests/src_models/trial.dot");
        assert_eq!(fsm.states().len(), 6);
    }

    /// FSM must be complete.
    #[rstest]
    #[case("tests/src_models/trial.dot")]
    #[case("tests/src_models/hypothesis_14.dot")]
    #[case("tests/src_models/hypothesis_21.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    fn fsm_is_complete(#[case] file_name: &str) {
        assert!(load_basic_fsm(file_name).is_complete());
    }
}
#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, DataSize, Serialize, Deserialize)]
pub struct MealySerde {
    /// States of the Mealy machine.
    states: Vec<State>,
    /// Initial state, usually State(0).
    initial_state: State,
    /// Set of `InputSymbol`.
    input_alphabet: Vec<InputSymbol>,
    /// Set of `OutputSymbol`.
    output_alphabet: Vec<OutputSymbol>,
    /// Mealy function
    trans_function: Vec<((State, InputSymbol), (State, OutputSymbol))>,
}

impl From<MealySerde> for Mealy {
    fn from(ser: MealySerde) -> Self {
        let MealySerde {
            states,
            initial_state,
            input_alphabet,
            output_alphabet,
            trans_function,
        } = ser;

        let trans_function = trans_function.into_iter().collect();
        let output_alphabet = output_alphabet.into_iter().collect();
        Self {
            states,
            initial_state,
            input_alphabet,
            output_alphabet,
            trans_function,
        }
    }
}

impl From<Mealy> for MealySerde {
    fn from(fsm: Mealy) -> Self {
        let Mealy {
            states,
            initial_state,
            input_alphabet,
            output_alphabet,
            trans_function,
        } = fsm;

        let trans_function = trans_function.into_iter().collect();
        let output_alphabet = output_alphabet.into_iter().collect();
        Self {
            states,
            initial_state,
            input_alphabet,
            output_alphabet,
            trans_function,
        }
    }
}
