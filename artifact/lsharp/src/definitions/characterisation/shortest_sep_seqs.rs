use std::collections::{BTreeMap, BTreeSet};

use itertools::Itertools;

use crate::definitions::mealy::{shortest_separating_sequence, InputSymbol, Mealy, State};
use crate::definitions::FiniteStateMachine;
use crate::util::data_structs::prefix_tree::PrefixTree;

use super::Characterisation;

/// Construct basic separating sequences for a given FSM.
///
/// This struct does not do anything special: we simply use a
/// breadth-first search to construct a characterisation set for
/// a (set of) state(s).
#[derive(Debug)]
pub struct SeparatingSeqs;

impl SeparatingSeqs {
    /// Compute an iterator for the characterisation set of `state`.
    ///
    /// The iterator contains sequences s.t. there is at least one sequence which separates
    /// `state` from another state in the FSM. The returned iterator has unique elements,
    ///  i.e., no sequence is repeated.
    ///
    /// # Example
    /// ```
    /// # use crate::lsharp_ru::{
    /// #    definitions::{
    /// #        mealy::{Mealy, State},
    /// #        FiniteStateMachine,
    /// #    },
    /// #      util::parsers::machine::read_mealy_from_file,
    /// # };
    /// # use lsharp_ru::definitions::characterisation::SeparatingSeqs;
    /// # use itertools::Itertools;
    /// let (fsm,_,_) = read_mealy_from_file("tests/src_models/trial.dot");
    /// let s0 = State::new(0);
    /// let states: Vec<_> = fsm.states().into_iter().filter(|s| *s != s0).collect();
    /// let char_set: Vec<_> = SeparatingSeqs::char_set_state(&fsm, s0);
    /// let separates = |x, seq| fsm.trace_from(x, seq) != fsm.trace_from(s0, seq);
    /// for x in states {
    ///        assert!(char_set.iter().any(|seq| separates(x, seq)));
    ///  }
    /// ```
    #[must_use]
    pub fn char_set_state(fsm: &Mealy, state: State) -> Vec<Vec<InputSymbol>> {
        let sep_seq_between = |(x, y)| shortest_separating_sequence(fsm, fsm, x, y);
        let other_states = fsm.states().into_iter().filter(move |s| *s != state);
        let unreduced_char_set = other_states
            .map(|y| (state, y))
            .filter_map(sep_seq_between)
            .map(|(in_seq, _)| in_seq);
        unreduced_char_set.unique().collect()
    }

    /// More general version of [`char_set_state`](Self::char_set_state),
    /// i.e., a char set for all states in `fsm`. This returns a maximal
    /// set of all separating sequences in an FSM.
    ///
    /// # Example
    /// ```
    /// # use crate::lsharp_ru::{
    /// #    definitions::{
    /// #        mealy::{Mealy, State},
    /// #        FiniteStateMachine,
    /// #    },
    /// #      util::parsers::machine::read_mealy_from_file,
    /// # };
    /// # use lsharp_ru::definitions::characterisation::SeparatingSeqs;
    /// # use itertools::Itertools;
    /// let (fsm,_,_) = read_mealy_from_file("tests/src_models/trial.dot");
    /// let states: Vec<_> = fsm.states().into_iter().collect();
    /// let char_set: Vec<_> = SeparatingSeqs::char_set(&fsm);
    /// let separates = |x, y, seq| fsm.trace_from(x, seq) != fsm.trace_from(y, seq);
    /// for (x, y) in Itertools::tuple_combinations(states.into_iter()) {
    ///        assert!(char_set.iter().any(|seq| separates(x, y, seq)));
    ///  }
    /// ```
    #[must_use]
    pub fn char_set(fsm: &Mealy) -> Vec<Vec<InputSymbol>> {
        let sep_seq_between = |(x, y)| shortest_separating_sequence(fsm, fsm, x, y);
        let pairs = fsm.states().into_iter().tuple_combinations();
        let char_set_unreduced = pairs.filter_map(sep_seq_between).map(|(iword, _)| iword);
        let prefix_tree = char_set_unreduced.collect::<PrefixTree>();
        prefix_tree.into_iter().collect()
    }
}

impl Characterisation for SeparatingSeqs {
    fn characterisation_map(fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>> {
        let mut ident = BTreeMap::<_, BTreeSet<Vec<InputSymbol>>>::default();
        let pairs = fsm.states().into_iter().tuple_combinations();
        let splits =
            |seq: Vec<InputSymbol>, s, t| fsm.trace_from(s, &seq).1 != fsm.trace_from(t, &seq).1;
        for seq in Self::char_set(fsm) {
            for (s, t) in pairs.clone() {
                if splits(seq.clone(), s, t) {
                    ident.entry(s).or_default().insert(seq.clone());
                    ident.entry(t).or_default().insert(seq.clone());
                }
            }
        }
        ident
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use rstest::rstest;

    use crate::definitions::characterisation::Characterisation;
    use crate::definitions::FiniteStateMachine;
    use crate::{definitions::mealy::Mealy, util::parsers::machine::read_mealy_from_file};

    use super::SeparatingSeqs;

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    /// Ensure that the initial state is set.
    #[rstest]
    #[case::coffee_machine("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    fn fsm_wp_separates(#[case] file_name: &str) {
        let fsm = load_basic_fsm(file_name);
        #[allow(clippy::needless_collect)]
        let states: Vec<_> = fsm.states().into_iter().collect();
        let char_set = SeparatingSeqs::char_set(&fsm);
        let separates = |x, y, seq| fsm.trace_from(x, seq).1 != fsm.trace_from(y, seq).1;
        for (x, y) in itertools::Itertools::tuple_combinations(states.into_iter()) {
            assert!(char_set.iter().any(|seq| separates(x, y, seq)));
        }
    }

    #[rstest]
    #[case::coffee_machine("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/BitVise.dot")]
    fn reproducible(#[case] file_name: &str) {
        let fsm = load_basic_fsm(file_name);
        let char_set = SeparatingSeqs::characterisation_map(&fsm);
        for _ in 0..5 {
            assert_eq!(char_set, SeparatingSeqs::characterisation_map(&fsm));
        }
    }
}
