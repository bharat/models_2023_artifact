use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};

use itertools::Itertools;
use rustc_hash::{FxHashMap, FxHashSet};
use thiserror::Error;

use std::collections::{HashMap, HashSet};
use std::convert::TryInto;
use std::fmt::Debug;
use std::fs::canonicalize;
use std::hash::BuildHasher;
use std::io::{self};
use std::path::Path;

use super::util::read_lines;

#[derive(Error, Debug)]
pub enum FSMParseError {
    #[error("File not found.")]
    FileNotFound(#[from] io::Error),
    #[error("Input symbol `{0}` is not defined in the input map.")]
    UndefinedInput(String),
    #[error("Output symbol `{0}` is not defined in the output map.")]
    UndefinedOutput(String),
}

/// Read a Mealy machine (FSM) from a DOT file using the provided maps.
///
/// # Panics
///
/// If file is not found or there was an error parsing the file.
///
/// # Errors
///
/// If an action was not present in the maps, or if we exceed
/// the number of states (2^31).
pub fn read_mealy_from_file_using_maps<P, S>(
    path: P,
    input_map: &HashMap<String, InputSymbol, S>,
    output_map: &HashMap<String, OutputSymbol, S>,
) -> Result<Mealy, FSMParseError>
where
    P: AsRef<Path> + Debug,
    S: BuildHasher,
{
    let path = canonicalize(&path).map_err(FSMParseError::from)?;
    let lines = read_lines(path).expect("Encountered error while reading file!");
    let mut states = HashSet::<State>::default();
    let mut initial_state = State::new(u32::MAX);
    let mut state_map = HashMap::<String, State>::default();
    let mut curr_state_max: i32 = -1;
    let mut trans_fn = FxHashMap::default();
    let mut state_map_fetch = |state_str| -> State {
        *state_map.entry(state_str).or_insert_with(|| {
            curr_state_max += 1;
            let ns = State::new(
                curr_state_max
                    .try_into()
                    .expect("We have exceeded 2^31 states."),
            );
            states.insert(ns);
            ns
        })
    };
    for line in lines.flatten() {
        if line.contains("__start0") && line.contains("->") {
            let mut init_line = line.split_whitespace().rev();
            let state_decl_initial = init_line.next().unwrap();
            let state_initial_str = &state_decl_initial[..state_decl_initial.len() - 1];
            initial_state = state_map_fetch(state_initial_str.to_string());
            continue;
        }
        if !line.contains("label") {
            continue;
        }

        if line.contains("->") {
            let (from_str, input_action, output_action, to_str) = extract_transition(&line);
            let from_state = state_map_fetch(from_str.to_string());
            if initial_state.raw() == u32::MAX {
                initial_state = from_state;
                log::info!("Selected state {} as initial!", from_str);
                assert_eq!(initial_state.raw(), 0);
            }
            let to_state = state_map_fetch(to_str.to_string());
            let input = *input_map
                .get(input_action)
                .ok_or(FSMParseError::UndefinedInput(input_action.into()))?;
            let output = *output_map
                .get(output_action)
                .ok_or(FSMParseError::UndefinedOutput(output_action.into()))?;
            trans_fn.insert((from_state, input), (to_state, output));
        }
    }
    let input_alphabet = trans_fn.keys().map(|k| k.1).unique().sorted().collect();
    let output_alphabet = trans_fn.values().map(|v| v.1).unique().collect();
    let ret = Mealy::new(
        states.into_iter().collect(),
        initial_state,
        input_alphabet,
        output_alphabet,
        trans_fn,
    );
    Ok(ret)
}

/// Read a Mealy machine (FSM) from a DOT file.
///
/// We also return the input and output maps for the mealy machine we have read.
///
/// # Panics
///
/// If file is not found or there was an error parsing the file.
pub fn read_mealy_from_file<P>(
    path: P,
) -> (
    Mealy,
    HashMap<String, InputSymbol>,
    HashMap<String, OutputSymbol>,
)
where
    P: AsRef<Path> + Debug,
{
    let path = canonicalize(&path).unwrap_or_else(|_| panic!("File {path:?} not found."));
    let lines = read_lines(path).expect("Encountered error while reading file!");
    let mut states = HashSet::<State>::default();
    let mut output_alphabet = FxHashSet::<OutputSymbol>::default();
    let mut input_map = HashMap::<String, InputSymbol>::default();
    let mut output_map = HashMap::<String, OutputSymbol>::default();
    let mut trans_function = HashMap::<(State, InputSymbol), State>::default();
    let mut output_function = HashMap::<(State, InputSymbol), OutputSymbol>::default();
    let mut initial_state = State::new(u32::MAX);
    let mut state_map = HashMap::<String, State>::default();
    let mut curr_state_max: i32 = -1;
    let mut trans_fn = FxHashMap::default();
    let mut curr_input_max: i32 = -1;
    let mut curr_output_max: i32 = -1;

    for line in lines.flatten() {
        if line.contains("__start0") && line.contains("->") {
            let mut init_line = line.split_whitespace().rev();
            let state_decl_initial = init_line.next().unwrap();
            let state_initial_str = &state_decl_initial[..state_decl_initial.len() - 1];
            if !state_map.contains_key(state_initial_str) {
                curr_state_max += 1;
                state_map.insert(
                    state_initial_str.to_string(),
                    State::new(curr_state_max.try_into().unwrap()),
                );
                states.insert(State::new(curr_state_max.try_into().unwrap()));
            }
            initial_state = *state_map.get(&state_initial_str.to_string()).unwrap();
            continue;
        }
        if !line.contains("label") {
            continue;
        }
        let mut state_map_fetch = |state_str| -> State {
            *state_map.entry(state_str).or_insert_with(|| {
                curr_state_max += 1;
                let ns = State::new(curr_state_max.try_into().unwrap());
                states.insert(ns);
                ns
            })
        };
        if line.contains("->") {
            let (from_str, input_action, output_action, to_str) = extract_transition(&line);
            let from_state = state_map_fetch(from_str.to_string());
            if initial_state.raw() == u32::MAX {
                initial_state = from_state;
                log::info!("Selected state {} as initial!", from_str);
                assert_eq!(initial_state.raw(), 0);
            }
            let to_state = state_map_fetch(to_str.to_string());
            let input = *input_map
                .entry(input_action.to_string())
                .or_insert_with(|| {
                    curr_input_max += 1;
                    InputSymbol::new(curr_input_max.try_into().unwrap())
                });
            let output = get_out_symbol(
                &mut output_map,
                output_action,
                &mut curr_output_max,
                &mut output_alphabet,
            );
            trans_function.insert((from_state, input), to_state);
            output_function.insert((from_state, input), output);
            trans_fn.insert((from_state, input), (to_state, output));
        }
    }
    let input_alphabet = input_map.values().copied().collect();

    let fsm = Mealy::new(
        states.into_iter().collect(),
        initial_state,
        input_alphabet,
        output_alphabet,
        trans_fn,
    );

    (fsm, input_map, output_map)
}

fn get_out_symbol(
    output_map: &mut HashMap<String, OutputSymbol>,
    output_action: &str,
    curr_output_max: &mut i32,
    output_alphabet: &mut HashSet<
        OutputSymbol,
        std::hash::BuildHasherDefault<rustc_hash::FxHasher>,
    >,
) -> OutputSymbol {
    let output = *output_map
        .entry(output_action.to_string())
        .or_insert_with(|| {
            *curr_output_max += 1;
            let out = OutputSymbol::new((*curr_output_max).try_into().unwrap());
            output_alphabet.insert(out);
            out
        });
    output
}

fn extract_transition(line: &str) -> (&str, &str, &str, &str) {
    let arr_idx = line.find("->").unwrap();
    let from_state = line[..arr_idx].trim();

    let begin_label_idx = line.find('[').unwrap();
    let to_state = line[arr_idx + 2..begin_label_idx].trim();

    let mut quote_idx = line.rmatch_indices('"');
    let close_quote_idx = quote_idx.next().unwrap().0;
    let open_quote_idx = quote_idx.next().unwrap().0;
    // let open_quote_idx = line.find(r#"""#).unwrap();
    // let close_quote_idx = line.rfind(r#"""#).unwrap();
    let sep_idx = line.find('/').unwrap();
    let input_str = line[open_quote_idx + 1..sep_idx].trim();
    let output_str = line[sep_idx + 1..close_quote_idx].trim();
    (from_state, input_str, output_str, to_state)
}
