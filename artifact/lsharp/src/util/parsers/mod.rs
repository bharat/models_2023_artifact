//! Parsers for logs and mealy machines (FSMs).
//!
//! - The logs must be in the [Abbadingo format](https://automata.cs.ru.nl/BenchmarkASMLRERS2019/Description).
//! - The FSMs must be in [`GraphViz` DOT format](https://automata.cs.ru.nl/Syntax/Mealy).
//! ### Note for FSMs
//! Our parser respects the `__start0 -> <state-id>` as the initial state over taking the
//! first state of the first transition in the listed in the file. That is, if both are set,
//! we consider the state marked with `__state0` as the initial state. If the start state
//! is not set, we use the initial state of the first transition.

/// Parser for logs.
pub mod logs;
/// Parser for FSMs.
pub mod machine;

pub(crate) mod util {
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    // The output is wrapped in a Result to allow matching on errors
    // Returns an Iterator to the Reader of the lines of the file.
    pub(crate) fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}
