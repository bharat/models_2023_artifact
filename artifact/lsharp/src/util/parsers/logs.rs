use std::collections::HashMap;
use std::fs::canonicalize;
use std::hash::BuildHasher;

use itertools::Itertools;

use crate::definitions::mealy::{InputSymbol, OutputSymbol};

use super::util::read_lines;

pub type LogResult = Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>;

/// Read the logs in a vector.
///
/// # Errors
/// If file at `file_path` cannot be read.
pub fn read<S: BuildHasher>(
    file_path: &str,
    input_map: &HashMap<String, InputSymbol, S>,
    output_map: &HashMap<String, OutputSymbol, S>,
) -> Result<LogResult, Box<dyn std::error::Error>> {
    let log_vec = read_lines(canonicalize(file_path)?)?
        .flatten()
        .skip(1) // The first entry contains the number of traces.
        .into_iter()
        .map(|line| parse_and_transform_trace(&line, input_map, output_map))
        .collect_vec();
    Ok(log_vec)
}

fn parse_and_transform_trace<S, I, O>(
    line: &str,
    input_map: &HashMap<String, I, S>,
    output_map: &HashMap<String, O, S>,
) -> (Vec<I>, Vec<O>)
where
    S: BuildHasher,
    I: Copy,
    O: Copy,
{
    let mut line_split_iter = line.split_whitespace();
    let _ = line_split_iter.next(); // Skip length
    let len_inputs: usize = (line_split_iter
        .next()
        .unwrap()
        .trim()
        .parse::<usize>()
        .expect("Error reading length of a log trace!"))
        / 2; // Skip acceptance
    let map_io_pair = |(i, o)| Option::zip(input_map.get(i), output_map.get(o));
    let (input_vec, output_vec): (Vec<_>, Vec<_>) = line_split_iter
        .batching(|line_split_iter| line_split_iter.next().zip(line_split_iter.next()))
        .map(|(i, o)| (i.trim(), o.trim()))
        .filter_map(map_io_pair)
        .unzip();
    assert_eq!(input_vec.len(), len_inputs, "Error while parsing the logs");
    (input_vec, output_vec)
}
