use clap::Parser;
use lsharp_ru::{
    learner::l_sharp::{Rule2, Rule3},
    oracles::equivalence::InfixStyle,
};

use crate::learning_config::EqOracle;

#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
pub struct Cli {
    #[arg(long, value_enum, default_value_t)]
    rule2: Rule2,
    #[arg(long, value_enum, default_value_t)]
    rule3: Rule3,
    #[arg(short = 'e', long)]
    eq_oracle: EqOracle,
    #[arg(short = 'm', long)]
    model: String,
    #[arg(short = 'x', long, default_value_t = 42)]
    seed: u64,
    #[arg(long)]
    eq_mode: InfixStyle,
    #[arg(short = 'k', long)]
    extra_states: usize,
    #[arg(short = 'l', long)]
    expected_random_length: usize,
    #[arg(long)]
    traces: Option<String>,
    #[arg(short = 'o', long)]
    out: String,
    #[arg(short = 'v')]
    /// Set verbosity level for logs.
    ///
    /// 0 => disabled, 1 => Info, 2 => Debug, 3 => Trace.
    verbosity: usize,
    #[arg(short = 'q', default_value_t)]
    /// Quiet mode (false by default).
    /// Do not print anything to stdio.
    quiet: bool,
    #[arg(long)]
    shuffle: bool,
    /// Cache rules per round of learning.
    #[arg(long, default_value_t)]
    cache_rules: bool,
    #[arg(long, default_value_t)]
    /// Compress the tree. Currently broken, do not use.
    compress_tree: bool,
}

// const  EO_HELP: &str = "This option sets the equivalence oracle to use. All options with the prefix `soucha` use FSMLIB, while `hads` uses hybrid-ads from Joshua Moerman. We also have a local implementation of H-ADS, called `hads_int`.";

// const ES_HELP: &str =
//     "Number of extra states you assume in the testing process. You do not need to add 1 to it.";

// const EXPECTED_INFIX_HELP : &str =
// "Expected length of the random infix. We will generate random infix sequences using a geometric distribution, where the probability of ending the sequence will be 1 divided by the argument provided.";
