# Seeds for failure cases proptest has generated in the past. It is
# automatically read and these particular cases re-run before any
# novel cases are generated.
#
# It is recommended to check this file in to source control so that
# everyone who runs the test benefits from these saved cases.
cc 9ad71cdf2f6b6389d574befe9b112645b9418cc4069e9c0080df70001469133a # shrinks to trial = ([InputSymbol(42), InputSymbol(19)], [State(1), State(11)])
cc b0a714217aa16b75df1c498f124d4839b1ccbe325ca73cba54b1cce162776d8d # shrinks to trial = ([InputSymbol(5), InputSymbol(5)], [State(0), State(10)])
