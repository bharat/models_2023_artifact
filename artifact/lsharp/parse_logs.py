#!/usr/bin/python3

import json
import os
import pathlib
import re
import sys
from typing import Any
import pandas as pd


# from expr_models import get_lsharp_command, run_expr

# REGEX to separate log file lines generated as set in logback.xml file in the cttlearning Java project

# Below, each REGEX part is presented with comments indicating the matching parts of the example above.
P_LOG = re.compile(
    r".*\|\s*(?P<key>[^:]+)\s*:\s*"  # cttlearning log format "Message key:"  (e.g., "ClosingStrategy:")
    r"(?P<val>.+)"  # cttlearning log format "Message value" (e.g., "CloseFirst")
)

# More information on the logback layout parameters used in:
# - cttlearning's logback.xml file: https://github.com/damascenodiego/cttlearn/blob/5fad701d1fffc1397aa49d7eb5fec56e8442f400/src/cttlearning/src/main/resources/logback.xml#L20
# - Logback official website:       https://logback.qos.ch/manual/layouts.html#conversionWord

(_, results_path, out_csv_name) = sys.argv


# results_path = "./log_comp_mem/"

logs_dict = {}
stats_overall = {
    "SUL name": [],
    "Seed": [],
    "Cache": [],
    "EquivalenceOracle": [],
    "Method": [],
    "Rounds": [],
    "MQ [Resets]": [],
    "MQ [Symbols]": [],
    "EQ [Resets]": [],
    "EQ [Symbols]": [],
    "Qsize": [],
    "Isize": [],
    "Equivalent": [],
    "Info": [],
}

overall_keys = [
    "SUL name",
    "Seed",
    "Cache",
    "CTT",
    "Extra States",
    "Random Infix Length",
    "EquivalenceOracle",
    "Method",
    "Rounds",
    "MQ [Resets]",
    "MQ [Symbols]",
    "EQ [Resets]",
    "EQ [Symbols]",
    "Qsize",
    "Isize",
    "OTreeSize",
    "Equivalent",
    "OTreeSize",
    "TreeType",
    "Testing queries/symbols",
    "EQStats",
    "Learning queries/symbols",
    "Learning [ms]"
]


def is_some(x: Any) -> bool:
    return x is not None


logfiles = pathlib.Path(results_path).expanduser().glob("*.*")


def dict_from_logpath(log):
    log_dict = dict()
    with open(cttl_log) as cttl_f:
        matches = map(P_LOG.match, cttl_f.readlines())
        for m in matches:
            if m == None:
                continue
            key, val = m.groups()
            key = key.strip()
            val = val.strip()
            if key == None or val == None:
                exit(-1)
            if key not in overall_keys:
                continue
            if key == "EQStats":
                val = val.replace("'", '"')
                hyp_size = json.loads(val)["HypSize"]
                if key not in log_dict:
                    log_dict[key] = [hyp_size]
                else:
                    old_val = log_dict[key]
                    old_val.append(hyp_size)
                    log_dict[key] = old_val
            elif key == "Testing queries/symbols" or key == "Learning queries/symbols":
                if key not in log_dict:
                    log_dict[key] = [val]
                else:
                    old_val = log_dict[key]
                    old_val.append(val)
                    log_dict[key] = old_val
            else:
                log_dict[key] = val
    errored = False
    for x in overall_keys:
        if x not in log_dict:
            errored = True
            # print("Logfile could not be read! : ")
            # print(log)
            # print(f"Key: {x}")
    if errored:
        print("Logfile could not be read! : ")
        model = log_dict["SUL name"][:-4]
        eo = log_dict["EquivalenceOracle"]
        seed = log_dict["Seed"]
        print(log)
        print(f"Model:  {model}, EO: {eo}, Seed: {seed}")
        # cmd = get_lsharp_command(model, "hads-int" if "H" in eo else "iads", seed)
        exit(-1)
        # os.remove(log)
        # run_expr(cmd)
    return log_dict


stats_overall = list()
for cttl_log in logfiles:
    log_dict = dict_from_logpath(cttl_log)
    stats_overall.append(log_dict)

df_overall = pd.DataFrame(stats_overall)
df_overall.rename(columns={"EQStats": "HypSize"}, inplace=True)
df_overall.to_csv(os.path.join(out_csv_name), index=False)