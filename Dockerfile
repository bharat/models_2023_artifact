# Setup stuff 
FROM rust:1.67

RUN apt-get -y update
RUN apt-get install -y sudo

RUN adduser --disabled-password --gecos '' sws
RUN adduser sws sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Install all the dependencies (excl python deps).
RUN sudo apt-get install -y cmake gcc python3 make build-essential git python3-pip

USER sws

ENV HOME /home/sws
ENV CARGO_ROOT /usr/local/cargo
ENV PATH $CARGO_ROOT/bin:$PATH

# Copy experiment files.
COPY . ${HOME}

RUN sudo chown -R sws /home/sws

# Intsall python deps.
RUN pip3 install --no-cache-dir --no-warn-script-location -r ${HOME}/requirements.txt

# Prepare things for execution.
WORKDIR ${HOME}/artifact/lsharp
RUN chmod +x run_me.sh

